import { LoginPage } from '../../pages/login.po';
import { browser, logging, ExpectedConditions } from 'protractor';





describe('1 - Tentar logar com campo senha vazio e mostrar mensagem de alerta', () => {
  let loginPage: LoginPage;
  beforeEach(() => {
    loginPage = new LoginPage();
    });
    it('Dado que estou na tela de login ConnectCont', async () => {
      await loginPage.navigateTo();
    });
      it('Quando Preencher campo email com usuário  "thiagosouza@50143"', async () => {
      await loginPage.prencherLogin("thiagosouza@50143", "");
    });
    it('E Clico no botão de conectar', async () => {
      await loginPage.Conectar();
    })
    it('Então Exibir mensagem de alerta no campo email "A senha é obrigatório"', async () => {
      await loginPage.ErrorPasswordEmpty();
    })
});


describe('2- Tentar logar com usuário inexistente e mostrar mensagem de alerta	', () => {
  let loginPage: LoginPage;
  beforeEach(() => {
    loginPage = new LoginPage();
    });
    it('Dado que estou na tela de login ConnectCont', async () => {
      await loginPage.navigateTo();
    });
      it('Quando preencher usuário com "thiagosouza@5000" e senha "79a534d0"', async () => {
      await loginPage.prencherLogin("thiagosouza@5000", "79a534d0");
    });
    it('E Clico no botão de conectar', async () => {
      await loginPage.Conectar();
    })
    it('Entao Exibir home do sistema com primeiro nome do usuário', async () => {
      await loginPage.nonexistentUser();
    })
});


describe('3- Tentar logar com campo usuário vazio e mostrar mensagem de alerta', () => {
  let loginPage: LoginPage;
  beforeEach(() => {
    loginPage = new LoginPage();
    });
    it('Dado que estou na tela de login ConnectCont', async () => {
      await loginPage.navigateTo();
    });
      it('Quando preencher usuário somente com a senha  "79a534d0"', async () => {
      await loginPage.prencherLogin("", "79a534d0");
    });
    it('E Clico no botão de conectar', async () => {
      await loginPage.Conectar();
    })
    it('Então Exibir mensagem de alerta no campo email "O usuário é obrigatório"', async () => {
      await loginPage.ErrorUserEmpty();
    })
});




describe(' 4- Tentar logar com usuário e senha incorreta e mostrar mensagem de erro', () => {
  let loginPage: LoginPage;
  beforeEach(() => {
    loginPage = new LoginPage();
    });
    it('Dado que estou na tela de login ConnectCont', async () => {
      await loginPage.navigateTo();
    });
      it('Quando preencher usuário com "thiagosouza@50143" e senha incorreta "000000"', async () => {
      await loginPage.prencherLogin("thiagosouza@50143", "000000");
    });
    it('E Clico no botão de conectar', async () => {
      await loginPage.Conectar();
    })
    it('Então Exibir mensagem de erro "Usuário inexistente ou senha inválida"', async () => {
      await loginPage.validarPopUpLoginErro()
    })
});


describe(' 5- Enviar solicitação de recuperação de conta com sucesso', () => {
  let loginPage: LoginPage;
  beforeEach(() => {
    loginPage = new LoginPage();
    });
    it('Dado que estou na tela de login ConnectCont', async () => {
      await loginPage.navigateTo();
    });
      it('Quando preencher clico em "não consegue acessar sua conta?" ', async () => {
      await loginPage.NaoConsegueAcessarConta();
    });
    it('E Clico em recuperar conta', async () => {
      await loginPage.RecuperarConta();
    })
    it('E preencho o codigo do usuario "50143" e o cpf "42922848817"', async () => {
      await loginPage.PreencherUsuarioECpf("50143","42922848817")
    })
    it('Entao clico em enviar e valido o modal de confirmação  ', async () => {
      await loginPage.ValidarMensagemRecuperacaoConta();
    })
   
});


describe(' 6- Logar no sistema com usuários e senha corretos', () => {
  let loginPage: LoginPage;
  beforeEach(() => {
    loginPage = new LoginPage();
    });
    it('Dado que estou na tela de login ConnectCont', async () => {
      await loginPage.navigateTo();
    });
      it('Quando preencher usuário com "thiagosouza@50143" e senha "f2d791f8"', async () => {
   
      await loginPage.prencherLogin("thiagosouza@50143", "f2d791f8");
    });
    it('E Clico no botão de conectar', async () => {
      await loginPage.Conectar();
    })
    it('Entao Exibir home do sistema com primeiro nome do usuário', async () => {
      await loginPage.ValidarTelaHome();
    })
});

