import { browser, by, element, ExpectedConditions } from 'protractor';

export class LoginPage {

  usuario = element(by.id('username'));
  CodigoApelidoPgRecuperar = element(by.xpath('//input[@formcontrolname="codigoOuApelido"]'));
  senha = element(by.id('password'));
  cpfInput = element(by.xpath('//input[@placeholder="CPF"]'));
  botaoConectar = element(by.xpath('//button[@class="sign-in__button-submit"]/span'))
  NaoConsegueAcessarSuaConta = element(by.xpath('//a[@routerlinkactive="router-link-active"]'))
  RecuperaConta = element(by.xpath('//div[@class="itens"]//a[1]'));
  EnviarpgRecuperar = element(by.xpath('//button[@class="btn-auth-primary"]'));
  EC = ExpectedConditions;


  async navigateTo() {
    const self = this;
    await browser.get('/').then(function () {
      // browser.wait(self.EC.textToBePresentInElement(self.botaoConectar, 'Connect'), 15000);
    });
  }
  async prencherLogin(usuario: string, senha: string) {
    await this.usuario.sendKeys(usuario);
    await this.senha.sendKeys(senha);
  }
  async Conectar() {
    const self = this;
    await this.botaoConectar.click().then(function () {

    });
  }
  async ValidarTelaHome() {
    const self = this;
    browser.wait(self.EC.textToBePresentInElement(element(by.xpath('//*[@id="js-nav-menu"]/li[2]/a/div/div/div/span')), 'Meus Clientes'), 200);
  }


  async validarPopUpLoginErro() {
    const self = this;
    browser.wait(self.EC.textToBePresentInElement(element(by.className('alert alert-danger ng-star-inserted')), 'Usuário inexistente ou senha inválida'));

  }



  async ErrorUserEmpty() {
    const self = this;
    browser.wait(self.EC.textToBePresentInElement(element(by.className('message-error ng-star-inserted')), 'O usuário é obrigatório'));

  }

  async ErrorPasswordEmpty() {
    const self = this;
    browser.wait(self.EC.textToBePresentInElement(element(by.className('message-error ng-star-inserted')), 'A senha é obrigatório'));

  }

  async nonexistentUser() {
    const self = this;
    browser.wait(self.EC.textToBePresentInElement(element(by.className('alert alert-danger ng-star-inserted')), 'Usuário inexistente ou senha inválida'));

  }

  async NaoConsegueAcessarConta() {
    const self = this;
    await this.NaoConsegueAcessarSuaConta.click().then(function () {

    });

  }
  async RecuperarConta() {
    const self = this;
    await this.RecuperaConta.click().then(function () {
    });
  }

  async PreencherUsuarioECpf(usuario: string, cpf: string) {
    await this.CodigoApelidoPgRecuperar.sendKeys(usuario);
    await this.cpfInput.sendKeys(cpf);
  }


  async ValidarMensagemRecuperacaoConta() {
    const self = this;
    await this.EnviarpgRecuperar.click().then(function () {
      browser.wait(self.EC.textToBePresentInElement(element(by.className('alert alert-success ng-star-inserted')), 'Uma nova senha foi gerada e em breve você irá recebê-la no email'));
    })

  }

}
