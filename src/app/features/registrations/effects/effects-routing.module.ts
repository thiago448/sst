import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { EffectsListComponent } from "./effects-list/effects-list.component";
import { EffectsFormComponent } from "./effects-form/effects-form.component";

const routes: Routes = [
  {
    path: "",
    component: EffectsListComponent,
    data: {
      breadcrumbs: [
        {
          title: "Cadastros",
          url: "/cadastros"
        },
        {
          title: "efeitos e Danos",
          url: "/cadastros/efeitos"
        }
      ]
    }
  },
  {
    path: "novo",
    component: EffectsFormComponent,
    data: {
      breadcrumbs: [
        {
          title: "Cadastros",
          url: "/cadastros"
        },
        {
          title: "efeitos e Danos",
          url: "/cadastros/efeitos"
        },
        {
          title: "novo",
          url: "/cadastros/efeitos/novo"
        }
      ]
    }
  },
  {
    path: "editar/:id",
    component: EffectsFormComponent,
    data: {
      breadcrumbs: [
        {
          title: "Cadastros",
          url: "/cadastros"
        },
        {
          title: "efeitos e Danos",
          url: "/cadastros/efeitos"
        },
        {
          title: "editar",
          url: "/cadastros/efeitos/editar"
        }
      ]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EffectsRoutingModule {}
