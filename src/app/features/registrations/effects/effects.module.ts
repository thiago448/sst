import { EmptyListModule } from "src/app/shared/empty-list/empty-list.module";
import { CheckboxModule } from "src/app/shared/checkbox/checkbox.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { EffectsRoutingModule } from "./effects-routing.module";
import { EffectsListComponent } from "./effects-list/effects-list.component";
import { EffectsFormComponent } from "./effects-form/effects-form.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [EffectsListComponent, EffectsFormComponent],
  imports: [
    CommonModule,
    EffectsRoutingModule,
    SharedModule,
    CheckboxModule,
    EmptyListModule
  ]
})
export class EffectsModule {}
