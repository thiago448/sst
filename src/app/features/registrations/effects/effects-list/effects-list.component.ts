import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-effects-list",
  templateUrl: "./effects-list.component.html",
  styleUrls: ["./effects-list.component.scss"]
})
export class EffectsListComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  newEffect() {
    this.router.navigate(["cadastros", "efeitos", "novo"]);
  }
}
