import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "riscos",
    loadChildren: () => import("./risks/risks.module").then(m => m.RisksModule)
  },
  {
    path: "efeitos",
    loadChildren: () =>
      import("./effects/effects.module").then(m => m.EffectsModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationsRoutingModule {}
