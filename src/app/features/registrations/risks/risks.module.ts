import { FeedbackModalsModule } from "./../../../shared/feedback-modals/feedback-modals.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { EmptyListModule } from "src/app/shared/empty-list/empty-list.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RisksRoutingModule } from "./risks-routing.module";
import { RiskListComponent } from "./risk-list/risk-list.component";
import { RisksFormComponent } from "./risks-form/risks-form.component";
import { SharedModule } from "src/app/shared/shared.module";
import { CheckboxModule } from "src/app/shared/checkbox/checkbox.module";
import { ButtonsModule } from "src/app/shared/buttons/buttons.module";
import { TagsModule } from "src/app/shared/tags/tags.module";
import { NgxDropzoneModule } from "ngx-dropzone";
import { SkeletonModule } from "src/app/shared/skeleton/skeleton.module";

@NgModule({
  declarations: [RiskListComponent, RisksFormComponent],
  imports: [
    CommonModule,
    RisksRoutingModule,
    SharedModule,
    CheckboxModule,
    EmptyListModule,
    ButtonsModule,
    TagsModule,
    NgxDropzoneModule,
    ReactiveFormsModule,
    FormsModule,
    FeedbackModalsModule,
    SkeletonModule,
  ],
})
export class RisksModule {}
