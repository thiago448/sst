import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RiskListComponent } from "./risk-list/risk-list.component";
import { RisksFormComponent } from "./risks-form/risks-form.component";

const routes: Routes = [
  {
    path: "",
    component: RiskListComponent,
    data: {
      breadcrumbs: [
        {
          title: "Cadastros",
          url: "/cadastros"
        },
        {
          title: "Riscos",
          url: "/cadastros/riscos"
        }
      ]
    }
  },
  {
    path: "novo",
    component: RisksFormComponent,
    data: {
      breadcrumbs: [
        {
          title: "Cadastros",
          url: "/cadastros"
        },
        {
          title: "Riscos",
          url: "/cadastros/riscos"
        },
        {
          title: "novo",
          url: "/cadastros/riscos/novo"
        }
      ]
    }
  },
  {
    path: "editar/:id",
    component: RisksFormComponent,
    data: {
      breadcrumbs: [
        {
          title: "Cadastros",
          url: "/cadastros"
        },
        {
          title: "Riscos",
          url: "/cadastros/riscos"
        },
        {
          title: "novo",
          url: "/cadastros/riscos/editar"
        }
      ]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RisksRoutingModule {}
