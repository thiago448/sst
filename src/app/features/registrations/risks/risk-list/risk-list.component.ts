import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-risk-list",
  templateUrl: "./risk-list.component.html",
  styleUrls: ["./risk-list.component.scss"]
})
export class RiskListComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  newEffect() {
    this.router.navigate(["cadastros", "riscos", "novo"]);
  }
}
