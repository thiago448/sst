import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RisksFormComponent } from './risks-form.component';

describe('RisksFormComponent', () => {
  let component: RisksFormComponent;
  let fixture: ComponentFixture<RisksFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RisksFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RisksFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
