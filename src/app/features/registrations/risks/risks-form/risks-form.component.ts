import { FeedbackModalComponent } from "./../../../../shared/feedback-modals/feedback-modal/feedback-modal.component";
import { Observable, of, empty } from "rxjs";
import { Validators, FormControl } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { Component, OnInit, ViewChild } from "@angular/core";
import {
  map,
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  tap,
  catchError,
} from "rxjs/operators";
import { RiskService } from "src/app/core/services/risk.service";

@Component({
  selector: "smart-risks-form",
  templateUrl: "./risks-form.component.html",
  styleUrls: ["./risks-form.component.scss"],
})
export class RisksFormComponent implements OnInit {
  public form: FormGroup;
  itemsResult = [{ id: 1, name: "options 1" }];

  resultRisco$: Observable<any> = of();
  resultRiscoDefault$: Observable<any> = of();

  @ViewChild(FeedbackModalComponent, { static: false })
  modalFeedback: FeedbackModalComponent;

  constructor(private fb: FormBuilder, private riskService: RiskService) {
    this.form = this.fb.group({
      risco: ["", Validators.compose([Validators.required])],
      codigoESocial: ["", Validators.compose([Validators.required])],
      fatorRiscoESocial: ["", Validators.compose([Validators.required])],
      grupoRisco: ["", Validators.compose([Validators.required])],
      nomeReduzido: ["", Validators.compose([Validators.required])],
      classiRisco: ["", Validators.compose([Validators.required])],
      uniMedida: ["", Validators.compose([Validators.required])],
      limitTolerencia: ["", Validators.compose([Validators.required])],
    });
  }

  onSubmitForm() {
    this.modalFeedback.showModal(false);
    if (!this.form.valid) {
      return this.validateAllFormFields(this.form);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsPristine({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    // this.resultRiscoDefault$ = this.riskService.searchUsers("gab").pipe(
    //   map((value) => value.items),
    //   map((items) =>
    //     items.map((item) => {
    //       return {
    //         ...item,
    //         name: item.login,
    //       };
    //     })
    //   )
    // );

    this.resultRisco$ = this.form.get("risco").valueChanges.pipe(
      map((value) => {
        try {
          return value.trim();
        } catch (error) {
          return "";
        }
      }),
      filter((value) => value.length > 1),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((value) => this.riskService.searchUsers(value)),

      map((value) => value.items),
      map((items) =>
        items.map((item) => {
          return {
            ...item,
            name: item.login,
          };
        })
      ),
      tap((result) => console.log(result)),
      catchError((error) => {
        console.log(error);
        return empty();
      })
    );
  }
}
