import { AlertComponent } from "ngx-bootstrap/alert/alert.component";
import { FormBuilder, Validators, FormControl } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "smart-recovery-account",
  templateUrl: "./recovery-account.component.html",
  styleUrls: ["./recovery-account.component.scss"],
})
export class RecoveryAccountComponent implements OnInit {
  public form: FormGroup;

  @Input()
  state;

  @Output()
  formSubmitChange = new EventEmitter();

  public alerts: any[] = [];

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      codigoOuApelido: ["", Validators.compose([Validators.required])],
      cpf: ["", Validators.compose([Validators.required])],
      enviarCopia: [false, Validators.compose([])],
    });
  }

  ngOnInit() {}

  get f() {
    return this.form.controls;
  }

  showMessage({ type, msg, timeout }) {
    this.alerts.push({
      type,
      msg,
      timeout,
    });
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.form.reset();
    this.alerts = this.alerts.filter((alert) => alert !== dismissedAlert);
  }

  formSubmit() {
    if (!this.form.valid) {
      return this.validateAllFormFields(this.form);
    }
    this.formSubmitChange.emit(this.form.value);
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsPristine({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
