import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RecoveryRoutingModule } from "./recovery-routing.module";
import { RecoveryComponent } from "./recovery.component";
import { SharedModule } from "src/app/shared/shared.module";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxMaskModule } from "ngx-mask";
import { TabsModule } from "ngx-bootstrap/tabs";
import { RecoveryPasswordComponent } from "./recovery-password/recovery-password.component";
import { RecoveryAccountComponent } from "./recovery-account/recovery-account.component";
import { AlertModule } from "ngx-bootstrap/alert";

@NgModule({
  declarations: [
    RecoveryComponent,
    RecoveryPasswordComponent,
    RecoveryAccountComponent
  ],
  imports: [
    CommonModule,
    RecoveryRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    TabsModule.forRoot(),
    NgxMaskModule.forRoot(),
    AlertModule.forRoot()
  ]
})
export class RecoveryModule {}
