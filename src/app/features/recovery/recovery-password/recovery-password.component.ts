import { FormBuilder, Validators, FormControl } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { AlertComponent } from "ngx-bootstrap/alert/alert.component";

@Component({
  selector: "smart-recovery-password",
  templateUrl: "./recovery-password.component.html",
  styleUrls: ["./recovery-password.component.scss"],
})
export class RecoveryPasswordComponent implements OnInit {
  private readonly USER_VALIDATION_REGEX: RegExp = new RegExp(
    "^[a-z0-9._%+-]+@[a-z0-9.-]"
  );
  public form: FormGroup;
  public alerts: any[] = [];
  @Input()
  state;

  @Output()
  formSubmitChange = new EventEmitter();

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      login: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(this.USER_VALIDATION_REGEX),
        ]),
      ],
      enviarCopia: [false, Validators.compose([])],
    });
  }

  ngOnInit() {}

  get f() {
    return this.form.controls;
  }

  showMessage({ type, msg, timeout }) {
    this.alerts.push({
      type,
      msg,
      timeout,
    });
  }

  formSubmit() {
    if (!this.form.valid) {
      return this.validateAllFormFields(this.form);
    }
    this.formSubmitChange.emit(this.form.value);
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.form.reset();
    this.alerts = this.alerts.filter((alert) => alert !== dismissedAlert);
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsPristine({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
