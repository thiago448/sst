import { RecoveryPasswordComponent } from "./recovery-password/recovery-password.component";
import { Router } from "@angular/router";
import { RecuperaContaOuSenhaModel } from "./../../core/models/recupera-conta-ou-senha-model";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Component, ViewChild } from "@angular/core";
import { AuthService } from "src/app/core/services/auth.service";
import { ToastrService } from "ngx-toastr";
import { RecoveryAccountComponent } from "./recovery-account/recovery-account.component";

@Component({
  selector: "smart-recovery",
  templateUrl: "./recovery.component.html",
  styleUrls: ["./recovery.component.scss"],
})
export class RecoveryComponent {
  public modalRef: BsModalRef;
  public formRecoveryPassword: FormGroup;
  public formRecoveryAccount: FormGroup;
  public errorMessage = null;
  public alerts: any[] = [];
  public config = {
    class: "modal-dialog-centered modal-recovery modal-sm",
  };
  public stateOptions = {
    RECOVERY_PASSWORD: "RECOVERY_PASSWORD",
    RECOVERY_ACCOUNT: "RECOVERY_ACCOUNT",
  };
  public error = null;
  public state = this.stateOptions.RECOVERY_PASSWORD;
  @ViewChild(RecoveryPasswordComponent, { static: false })
  recoveryPasswordComponent: RecoveryPasswordComponent;

  @ViewChild(RecoveryAccountComponent, { static: false })
  recoveryAccountComponent: RecoveryAccountComponent;

  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private authService: AuthService,
    private router: Router
  ) {
    this.formRecoveryPassword = this.formBuilder.group({
      login: ["", Validators.compose([Validators.required])],
      enviarCopia: ["", Validators.compose([Validators.required])],
    });

    this.formRecoveryAccount = this.formBuilder.group({
      codigoOuApelido: ["", Validators.compose([Validators.required])],
      cpf: ["", Validators.compose([Validators.required])],
      enviarCopia: ["", Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    this.modalService.onHidden.subscribe(() => console.log("fechou"));
  }

  get formRecovery() {
    return this.formRecoveryPassword.controls;
  }

  get formRecoveryTwo() {
    return this.formRecoveryAccount.controls;
  }

  changeProblem(option) {
    this.state = this.stateOptions[option];
  }

  generateModel(value: any): RecuperaContaOuSenhaModel {
    let data: RecuperaContaOuSenhaModel = new RecuperaContaOuSenhaModel();
    data = Object.assign(data, value);
    return data;
  }

  formSubmitAccount(event) {
    const data = this.generateModel(event);
    this.authService.recoveryAccount(data).subscribe(
      () => {
        this.showAlertSuccess();
      },
      (error) => {
        this.showAlertError();
      }
    );
  }

  formSubmitPassword(event) {
    const data = this.generateModel(event);
    this.authService.recoveryPassword(data).subscribe(
      () => {
        this.showAlertSuccess();
      },
      (error) => {
        this.showAlertError();
      }
    );
  }

  closeModal() {
    this.modalRef.hide();
  }

  showAlertError() {
    this.alerts.push({
      type: "danger",
      msg: "Usuário informado não existe",
      // timeout: 20000,
    });
  }
  showAlertSuccess() {
    this.alerts.push({
      type: "success",
      msg: "Uma nova senha foi gerada e em breve você irá recebê-la no email",
      // timeout: 20000,
    });
  }
}
