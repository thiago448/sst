import { FunctionFormComponent } from "./function-form/function-form.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CustomersComponent } from "./customers.component";
import { CustomerFormComponent } from "./customer-form/customer-form.component";
import { TabsComponent } from "./tabs/tabs.component";
import { SectorsListComponent } from "./sectors-list/sectors-list.component";
import { WorkspaceFormComponent } from "./workspace-form/workspace.form-component";
import { EmployeesListComponent } from "./employees-list/employees-list.component";
import { EmployeeFormComponent } from "./employee-form/employee-form.component";
import { SectorFormComponent } from "./sector-form/sector-form.component";
import { WorkspacesListComponent } from "./workspaces-list/workspaces-list.component";
import { FunctionsListComponent } from "./functions-list/functions-list.component";

const clientBreadcrumb = {
  title: "Clientes",
  url: "/clientes",
};

const sectorsBreadcrumb = {
  title: "Setores",
  url: "/clientes/setores",
};

const functionsBreadcrumb = {
  title: "Cargos",
  url: "/clientes/cargos",
};

const workspaceBreadcrumb = {
  title: "Ambientes de Trabalho",
  url: "/clientes/ambientes-trabalho",
};

const employeesBreadcrumb = {
  title: "Funcionários",
  url: "/clientes/funcionarios",
};

const routes: Routes = [
  {
    path: "",
    component: CustomersComponent,
    pathMatch: "full",
    data: {
      breadcrumbs: [clientBreadcrumb],
    },
  },
  {
    path: "",
    component: TabsComponent,
    children: [
      {
        path: "setores",
        children: [
          {
            path: "",
            pathMatch: "full",
            component: SectorsListComponent,
            data: {
              breadcrumbs: [clientBreadcrumb, sectorsBreadcrumb],
            },
          },
          {
            path: "novo",
            component: SectorFormComponent,
            data: {
              breadcrumbs: [
                clientBreadcrumb,
                sectorsBreadcrumb,
                { title: "Novo Setor", url: "clientes/setores/novo" },
              ],
            },
          },
          {
            path: "editar/:id",
            component: SectorFormComponent,
            data: {
              breadcrumbs: [
                clientBreadcrumb,
                sectorsBreadcrumb,
                { title: "Editar Setor", url: "" },
              ],
            },
          },
        ],
      },
      {
        path: "editar/:id",
        component: CustomerFormComponent,
        data: { breadcrumbs: [clientBreadcrumb, "Novo Cliente"] },
      },
      {
        path: "novo",
        component: CustomerFormComponent,
        data: {
          breadcrumbs: [
            clientBreadcrumb,
            { title: "Novo", url: "clientes/novo" },
          ],
        },
      },
      {
        path: "funcionarios",
        children: [
          {
            path: "",
            pathMatch: "full",
            component: EmployeesListComponent,
            data: {
              breadcrumbs: [clientBreadcrumb, employeesBreadcrumb],
            },
          },
          {
            path: "novo",
            component: EmployeeFormComponent,
            data: {
              breadcrumbs: [
                clientBreadcrumb,
                employeesBreadcrumb,
                { title: "Novo", url: "clientes/funcionarios" },
              ],
            },
          },
          {
            path: "editar/:id",
            component: EmployeeFormComponent,
            data: {
              breadcrumbs: [
                clientBreadcrumb,
                employeesBreadcrumb,
                { title: "Editar Funcionários", url: "clientes/funcionarios" },
              ],
            },
          },
        ],
      },
      {
        path: "ambientes-trabalho",
        children: [
          {
            path: "",
            pathMatch: "full",
            component: WorkspacesListComponent,
            data: {
              breadcrumbs: [clientBreadcrumb, workspaceBreadcrumb],
            },
          },
          {
            path: "novo",
            component: WorkspaceFormComponent,
            data: {
              breadcrumbs: [
                clientBreadcrumb,
                workspaceBreadcrumb,
                {
                  title: "novo",
                  url: "clientes/ambientes-trabalho/novo",
                },
              ],
            },
          },
          {
            path: "editar/:id",
            component: WorkspaceFormComponent,
            data: {
              breadcrumbs: [
                clientBreadcrumb,
                workspaceBreadcrumb,
                {
                  title: "editar Ambientes de Trabalho",
                  url: "clientes/ambientes-trabalho/novo",
                },
              ],
            },
          },
        ],
      },
      {
        path: "cargos",
        children: [
          {
            path: "",
            pathMatch: "full",
            component: FunctionsListComponent,
            data: {
              breadcrumbs: [clientBreadcrumb, functionsBreadcrumb],
            },
          },
          {
            path: "novo",
            component: FunctionFormComponent,
            data: {
              breadcrumbs: [
                clientBreadcrumb,
                functionsBreadcrumb,
                {
                  title: "novo",
                  url: "clientes/cargos/novo",
                },
              ],
            },
          },
          {
            path: "editar/:id",
            component: FunctionFormComponent,
            data: {
              breadcrumbs: [
                clientBreadcrumb,
                functionsBreadcrumb,
                {
                  title: "editar Funções/Cargos",
                  url: "clientes/cargos/novo",
                },
              ],
            },
          },
        ],
      },
    ],
  },
  {
    path: "**",
    component: CustomersComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomersRoutingModule {}
