import { Component, OnInit } from "@angular/core";
import { deLocale } from "ngx-bootstrap/locale";
// import { BsLocaleService } from "ngx-bootstrap/locale/";

import { defineLocale } from "ngx-bootstrap/chronos";
import { ptBrLocale } from "ngx-bootstrap/locale";
defineLocale("pt-br", ptBrLocale);

@Component({
  selector: "smart-employee-form",
  templateUrl: "./employee-form.component.html",
  styleUrls: ["./employee-form.component.scss"],
})
export class EmployeeFormComponent implements OnInit {
  locale = "pt-br";

  // constructor(private _localeService: BsLocaleService) {
  //   _localeService.use("pt-br");
  // }
  ngOnInit() {}
}
