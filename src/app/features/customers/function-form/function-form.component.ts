import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Validators } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { Component, OnInit, TemplateRef } from "@angular/core";

@Component({
  selector: "smart-function-form",
  templateUrl: "./function-form.component.html",
  styleUrls: ["./function-form.component.scss"],
})
export class FunctionFormComponent implements OnInit {
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      name: ["", Validators.compose([Validators.required])],
      cbo: ["", Validators.compose([Validators.required])],
      gpName: ["", Validators.compose([Validators.required])],
      gpDesc: ["", Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {}
}
