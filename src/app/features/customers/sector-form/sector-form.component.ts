import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

@Component({
  selector: "smart-sector-form",
  templateUrl: "./sector-form.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ["./sector-form.component.scss"]
})
export class SectorFormComponent implements OnInit {
  form: FormGroup;
  errorsMessage = {
    required: "O campo nome é obrigatório",
    email: "O campo email é obrigatório"
  };

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      name: ["", Validators.compose([Validators.required, Validators.email])],
      desc: ["", Validators.compose([Validators.required])]
    });
  }
  ngOnInit() {
    this.form.valueChanges.subscribe(value => {});
  }
  onSubmit() {
    if (this.form.valid) {
    } else {
      this.validateAllFormFields(this.form);
    }
  }
  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }
  fieldErrors(field: string) {
    return this.form.get(field).errors;
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  displayFieldCss(field: string) {
    return {
      error: this.isFieldValid(field)
    };
  }
}
