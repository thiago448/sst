import { FormNewGroupActivityModule } from "./../../shared/form-new-group-activity/form-new-group-activity.module";
import { FormFieldModule } from "./../../shared/form-field/form-field.module";
import { FeedbackModalsModule } from "./../../shared/feedback-modals/feedback-modals.module";
import { ButtonsGroupModule } from "./../../shared/buttons-group/buttons-group.module";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalDeleteModule } from "./../../shared/modal-delete/modal-delete.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CustomersRoutingModule } from "./customers-routing.module";
import { CustomersComponent } from "./customers.component";
import { CustomerFormComponent } from "./customer-form/customer-form.component";
import { SharedModule } from "src/app/shared/shared.module";
import { NgxMaskModule } from "ngx-mask";
import { PanelsModule } from "../../shared/panels/panels.module";
import { TabsComponent } from "./tabs/tabs.component";
import { SectorsListComponent } from "./sectors-list/sectors-list.component";
import { WorkspaceFormComponent } from "./workspace-form/workspace.form-component";
import { EmployeesListComponent } from "./employees-list/employees-list.component";
import { SectorFormComponent } from "./sector-form/sector-form.component";
import { WorkspacesListComponent } from "./workspaces-list/workspaces-list.component";
import { EmployeeFormComponent } from "./employee-form/employee-form.component";
import { FunctionFormComponent } from "./function-form/function-form.component";
import { FunctionsListComponent } from "./functions-list/functions-list.component";
import { RadioButtonModule } from "src/app/shared/radio-button/radio-button.module";
import { TagsModule } from "src/app/shared/tags/tags.module";
import { NgxDropzoneModule } from "ngx-dropzone";
import { ModalDetailsModule } from "src/app/shared/modal-details/modal-details.module";
import { FormNewFunctionModule } from "src/app/shared/form-new-function/form-new-function.module";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { UtilsModule } from "src/app/shared/utils/utils.module";
import { ModalModule } from "ngx-bootstrap/modal";
import { VirtualScrollerModule } from "ngx-virtual-scroller";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { SkeletonModule } from "src/app/shared/skeleton/skeleton.module";

@NgModule({
  declarations: [
    CustomersComponent,
    CustomerFormComponent,
    TabsComponent,
    SectorsListComponent,
    SectorFormComponent,
    WorkspaceFormComponent,
    EmployeesListComponent,
    EmployeeFormComponent,
    SectorFormComponent,
    WorkspacesListComponent,
    FunctionFormComponent,
    FunctionsListComponent,
  ],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    SharedModule,
    PanelsModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    TagsModule,
    NgxDropzoneModule,
    ModalDetailsModule,
    ModalDeleteModule,
    FormNewFunctionModule,
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    UtilsModule,
    ButtonsGroupModule,
    FeedbackModalsModule,
    FormFieldModule,
    FormNewGroupActivityModule,
    ModalModule.forRoot(),
    VirtualScrollerModule,
    InfiniteScrollModule,
    SkeletonModule,
  ],
})
export class CustomersModule {}
