import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-sectors",
  templateUrl: "./sectors-list.component.html",
  styleUrls: ["./sectors-list.component.scss"]
})
export class SectorsListComponent implements OnInit {
  constructor(private router: Router) {}
  ngOnInit() {}
  newSector() {
    this.router.navigate(["clientes", "setores", "novo"]);
  }
}
