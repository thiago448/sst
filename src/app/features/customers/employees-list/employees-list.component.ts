import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-employees-list",
  templateUrl: "./employees-list.component.html",
  styleUrls: ["./employees-list.component.scss"]
})
export class EmployeesListComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  newEmployee() {
    this.router.navigate(["clientes", "funcionarios", "novo"]);
  }
}
