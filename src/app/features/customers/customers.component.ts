import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { ViewChildren, ViewChild, ChangeDetectorRef } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import { RiskService } from "src/app/core/services/risk.service";
import { IPageInfo, VirtualScrollerComponent } from "ngx-virtual-scroller";
import { CdkVirtualScrollViewport } from "@angular/cdk/scrolling";
import { auditTime, tap } from "rxjs/operators";

interface ListItem {
  name: string;
  lastName: string;
}

@Component({
  selector: "smart-customers",
  templateUrl: "./customers.component.html",
  styleUrls: ["./customers.component.scss"],
})
export class CustomersComponent implements OnInit {
  constructor(
    private router: Router,
    private riskService: RiskService,
    private ref: ChangeDetectorRef
  ) {}
  scrollDistance = 1;
  scrollUpDistance = 2;
  public buffer = [];
  public page: number = 1;
  public totalPages: number = 5;
  public limit: 5;

  ngOnInit() {
    this.riskService.searchUsers(this.page, this.limit).subscribe((data) => {
      console.log(data);
      this.buffer = data;
      this.ref.detectChanges();
    });
  }

  newClient() {
    this.router.navigate(["clientes", "novo"]);
  }

  onScrollDown(ev) {
    this.riskService.searchUsers(this.page + 1).subscribe((data) => {
      this.buffer = this.buffer.concat(data);
      this.page = this.page + 1;
      this.ref.detectChanges();
    });
  }
}
