import { FeedbackModalComponent } from "./../../../shared/feedback-modals/feedback-modal/feedback-modal.component";
import { ModalDetailsComponent } from "./../../../shared/modal-details/modal-details/modal-details.component";
import { Component, OnInit, ChangeDetectorRef, ViewChild } from "@angular/core";
import { ModalDeleteComponent } from "src/app/shared/modal-delete/modal-delete.component";
import { ToastrService } from "ngx-toastr";
import { ToastComponent } from "src/app/shared/toast/toast.component";

@Component({
  selector: "smart-new-customer",
  templateUrl: "./customer-form.component.html",
  styleUrls: ["./customer-form.component.scss"],
})
export class CustomerFormComponent implements OnInit {
  constructor(public toastr: ToastrService, private ref: ChangeDetectorRef) {}

  @ViewChild(ModalDetailsComponent, { static: false })
  modalAddress: ModalDetailsComponent;

  radioModel = "Middle";

  @ViewChild(ModalDeleteComponent, { static: false })
  modalDelete: ModalDeleteComponent;

  @ViewChild(FeedbackModalComponent, { static: false })
  modalFeedbackSuccess: FeedbackModalComponent;

  files: File[] = [];
  name = "Angular 4";
  url: any = null;

  onSelect(event) {}

  openModalAddress() {
    this.modalAddress.showModal();
  }

  openToast() {
    this.toastr.info("aaa", "a", {
      timeOut: 10000000,
      extendedTimeOut: 100000000,
      closeButton: true,

      toastComponent: ToastComponent,
    });
  }

  closeModalAddress() {
    this.modalAddress.hideModal();
  }

  openModalFeeback() {
    this.modalFeedbackSuccess.showModal();
  }

  closeModalFeeback() {
    this.modalFeedbackSuccess.hideModal();
  }

  openModalDelete() {
    this.modalDelete.showModal();
  }

  closeModalDelete() {
    this.modalDelete.hideModal();
  }

  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  ngOnInit() {}
}

interface FileReaderEventTarget extends EventTarget {
  result: string;
}

interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
}
