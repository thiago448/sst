import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-functions-list",
  templateUrl: "./functions-list.component.html",
  styleUrls: ["./functions-list.component.scss"]
})
export class FunctionsListComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  newFunction() {
    this.router.navigate(["clientes", "cargos", "novo"]);
  }
}
