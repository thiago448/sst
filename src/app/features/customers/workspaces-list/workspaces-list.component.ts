import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-workspaces-list",
  templateUrl: "./workspaces-list.component.html",
  styleUrls: ["./workspaces-list.component.scss"]
})
export class WorkspacesListComponent implements OnInit {
  constructor(private router: Router) {}
  ngOnInit() {}
  newWorkspace() {
    this.router.navigate(["clientes", "ambientes-trabalho", "novo"]);
  }
}
