import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterRiskStepComponent } from './register-risk-step.component';

describe('RegisterRiskStepComponent', () => {
  let component: RegisterRiskStepComponent;
  let fixture: ComponentFixture<RegisterRiskStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterRiskStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterRiskStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
