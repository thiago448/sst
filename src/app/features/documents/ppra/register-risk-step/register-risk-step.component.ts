import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Component, OnInit, TemplateRef } from "@angular/core";

@Component({
  selector: "smart-register-risk-step",
  templateUrl: "./register-risk-step.component.html",
  styleUrls: ["./register-risk-step.component.scss"],
})
export class RegisterRiskStepComponent implements OnInit {
  modalRefAddExistFunction: BsModalRef;
  modalRefAddNewFunction: BsModalRef;
  modalRefMeteringEquipment: BsModalRef;
  modalRefRiskModal: BsModalRef;
  modalRefEmployeesList: BsModalRef;
  modalRefControlEpi: BsModalRef;
  modalRefAddNewGroupActivity: BsModalRef;
  modalRefAddGheList: BsModalRef;
  modalRefAddNewRisk: BsModalRef;
  modalConfig = {
    keyboard: true,
    class: "modal-lg modal-dialog-centered",
  };

  constructor(private modalService: BsModalService) {}

  ngOnInit() {}

  isDropupButtonsNewFunction = true;
  isDropDownTable = true;

  openMeteringEquipment(template: TemplateRef<any>) {
    this.modalRefMeteringEquipment = this.modalService.show(
      template,
      this.modalConfig
    );
  }

  openModalGheList(template: TemplateRef<any>) {
    this.modalRefAddGheList = this.modalService.show(
      template,
      this.modalConfig
    );
  }

  openModalEmployeesList(template: TemplateRef<any>) {
    this.modalRefEmployeesList = this.modalService.show(
      template,
      this.modalConfig
    );
  }

  openModalNewRiskModal(template: TemplateRef<any>) {
    this.modalRefAddNewRisk = this.modalService.show(
      template,
      this.modalConfig
    );
  }

  openModalControlEpi(template: TemplateRef<any>) {
    this.modalRefRiskModal = this.modalService.show(template, this.modalConfig);
  }

  openModalRisk(template: TemplateRef<any>) {
    this.modalRefRiskModal = this.modalService.show(template, this.modalConfig);
  }

  openModalAddNewFunction(template: TemplateRef<any>) {
    this.modalRefAddExistFunction = this.modalService.show(
      template,
      this.modalConfig
    );
  }
  openModalAddExistFunction(template: TemplateRef<any>) {
    this.modalRefAddNewFunction = this.modalService.show(
      template,
      this.modalConfig
    );
  }
  openModalAddNewGroupActivity(template: TemplateRef<any>) {
    this.modalRefAddNewGroupActivity = this.modalService.show(
      template,
      this.modalConfig
    );
  }
}
