import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GheListModalComponent } from './ghe-list-modal.component';

describe('GheListModalComponent', () => {
  let component: GheListModalComponent;
  let fixture: ComponentFixture<GheListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GheListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GheListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
