import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlEPIModalComponent } from './control-epi-modal.component';

describe('ControlEPIModalComponent', () => {
  let component: ControlEPIModalComponent;
  let fixture: ComponentFixture<ControlEPIModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlEPIModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlEPIModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
