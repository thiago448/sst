import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-control-epi-modal",
  templateUrl: "./control-epi-modal.component.html",
  styleUrls: ["./control-epi-modal.component.scss"],
})
export class ControlEPIModalComponent implements OnInit {
  constructor() {}
  showListEPI = false;
  ngOnInit() {}
  toggleButton() {
    this.showListEPI = !this.showListEPI;
  }
}
