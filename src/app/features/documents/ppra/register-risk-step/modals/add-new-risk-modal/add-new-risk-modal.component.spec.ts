import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewRiskModalComponent } from './add-new-risk-modal.component';

describe('AddNewRiskModalComponent', () => {
  let component: AddNewRiskModalComponent;
  let fixture: ComponentFixture<AddNewRiskModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewRiskModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewRiskModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
