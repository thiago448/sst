import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewActiviteComponent } from './add-new-activite.component';

describe('AddNewActiviteComponent', () => {
  let component: AddNewActiviteComponent;
  let fixture: ComponentFixture<AddNewActiviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewActiviteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewActiviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
