import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddExistFunctionModalComponent } from './add-exist-function-modal.component';

describe('AddExistFunctionModalComponent', () => {
  let component: AddExistFunctionModalComponent;
  let fixture: ComponentFixture<AddExistFunctionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddExistFunctionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddExistFunctionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
