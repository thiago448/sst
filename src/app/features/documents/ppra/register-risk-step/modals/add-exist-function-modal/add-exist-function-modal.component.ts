import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "smart-add-exist-function-modal",
  templateUrl: "./add-exist-function-modal.component.html",
  styleUrls: ["./add-exist-function-modal.component.scss"],
})
export class AddExistFunctionModalComponent implements OnInit {
  @Output()
  openSecondModalEvent = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  openSecondModal() {
    this.openSecondModalEvent.emit();
  }
}
