import { Component, OnInit, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "smart-risk-modal",
  templateUrl: "./risk-modal.component.html",
  styleUrls: ["./risk-modal.component.scss"],
})
export class RiskModalComponent implements OnInit {
  @Output()
  openSecondModalEvent = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}
}
