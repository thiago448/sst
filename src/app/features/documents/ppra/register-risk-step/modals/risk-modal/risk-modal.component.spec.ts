import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskModalComponent } from './risk-modal.component';

describe('RiskModalComponent', () => {
  let component: RiskModalComponent;
  let fixture: ComponentFixture<RiskModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
