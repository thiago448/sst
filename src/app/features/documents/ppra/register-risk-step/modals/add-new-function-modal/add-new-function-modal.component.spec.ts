import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewFunctionModalComponent } from './add-new-function-modal.component';

describe('AddNewFunctionModalComponent', () => {
  let component: AddNewFunctionModalComponent;
  let fixture: ComponentFixture<AddNewFunctionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewFunctionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewFunctionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
