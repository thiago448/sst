import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-employees-list-modal",
  templateUrl: "./employees-list-modal.component.html",
  styleUrls: ["./employees-list-modal.component.scss"],
})
export class EmployeesListModalComponent implements OnInit {
  constructor() {}
  showFormAddNewEmployee = false;
  showFormAddExistEmployee = false;
  ngOnInit() {}

  showFormAddNew() {
    this.showFormAddExistEmployee = false;
    this.showFormAddNewEmployee = true;
  }
  showFormAddExist() {
    this.showFormAddNewEmployee = false;
    this.showFormAddExistEmployee = true;
  }
}
