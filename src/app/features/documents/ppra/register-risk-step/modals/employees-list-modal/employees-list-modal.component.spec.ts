import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesListModalComponent } from './employees-list-modal.component';

describe('EmployeesListModalComponent', () => {
  let component: EmployeesListModalComponent;
  let fixture: ComponentFixture<EmployeesListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
