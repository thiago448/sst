import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeteringEquipmentModalComponent } from './metering-equipment-modal.component';

describe('MeteringEquipmentModalComponent', () => {
  let component: MeteringEquipmentModalComponent;
  let fixture: ComponentFixture<MeteringEquipmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeteringEquipmentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeteringEquipmentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
