import { PpraStepsComponent } from "./ppra-steps/ppra-steps.component";

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PpraListComponent } from "./ppra-list/ppra-list.component";
import { ViewPpraComponent } from "./view-ppra/view-ppra.component";

const routes: Routes = [
  {
    path: "",
    component: PpraListComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "ppra's",
          url: "/documentos/ppra",
        },
      ],
    },
  },
  {
    path: "novo",
    component: PpraStepsComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "ppra's",
          url: "/documentos/ppra",
        },
        {
          title: "novo",
          url: "/documentos/ppra/novo",
        },
      ],
    },
  },
  {
    path: "editar",
    component: PpraStepsComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "ppra's",
          url: "/documentos/ppra",
        },
        {
          title: "novo",
          url: "/documentos/ppra/novo",
        },
      ],
    },
  },
  {
    path: "view",
    component: ViewPpraComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "ppra's",
          url: "/documentos/ppra",
        },
        {
          title: "novo",
          url: "/documentos/ppra/novo",
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PPRARoutingModule {}
