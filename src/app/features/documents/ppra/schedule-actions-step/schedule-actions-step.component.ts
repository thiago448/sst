import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import {
  Component,
  OnInit,
  TemplateRef,
  ChangeDetectorRef,
} from "@angular/core";
import {
  BsDatepickerConfig,
  BsDatepickerViewMode,
} from "ngx-bootstrap/datepicker";

@Component({
  selector: "smart-schedule-actions-step",
  templateUrl: "./schedule-actions-step.component.html",
  styleUrls: ["./schedule-actions-step.component.scss"],
})
export class ScheduleActionsStepComponent implements OnInit {
  modalRef: BsModalRef;
  modalConfig = {
    keyboard: true,
    class: "modal-lg modal-dialog-centered",
  };
  bsValue: Date = new Date(2017, 7);
  minMode: BsDatepickerViewMode = "month";

  bsConfig: Partial<BsDatepickerConfig>;

  public dates = [];

  constructor(
    private modalService: BsModalService,
    private changeDetection: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.bsConfig = Object.assign(
      {},
      {
        minMode: this.minMode,
      }
    );
  }

  onValueChangeDate(date) {
    this.dates = [...this.dates, date];
    console.log("entrou");
    console.log(this.dates);
    this.changeDetection.detectChanges();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.modalConfig);
  }
}
