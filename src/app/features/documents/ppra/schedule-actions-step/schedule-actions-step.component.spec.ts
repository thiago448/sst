import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleActionsStepComponent } from './schedule-actions-step.component';

describe('ScheduleActionsStepComponent', () => {
  let component: ScheduleActionsStepComponent;
  let fixture: ComponentFixture<ScheduleActionsStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleActionsStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleActionsStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
