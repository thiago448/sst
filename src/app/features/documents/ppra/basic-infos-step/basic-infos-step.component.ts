import { Component, OnInit, Input } from "@angular/core";

import { FormGroup } from "@angular/forms";
import { defineLocale } from "ngx-bootstrap/chronos";
import { ptBrLocale } from "ngx-bootstrap/locale";
defineLocale("pt-br", ptBrLocale);

@Component({
  selector: "smart-basic-infos-step",
  templateUrl: "./basic-infos-step.component.html",
  styleUrls: ["./basic-infos-step.component.scss"],
})
export class BasicInfosStepComponent implements OnInit {
  public customers: Array<any> = [
    {
      id: 1,
      name: "Empresa 1",
    },
    {
      id: 2,
      name: "empresa 2",
    },
    {
      id: 3,
      name: "empresa 3",
    },
    {
      id: 4,
      name: "empresa 4",
    },
    {
      id: 5,
      name: "empresa 5",
    },
  ];

  @Input() form: FormGroup;

  ngOnInit() {}
  locale = "pt-br";

  // constructor(private _localeService: BsLocaleService) {
  //   _localeService.use("pt-br");
  // }
}
