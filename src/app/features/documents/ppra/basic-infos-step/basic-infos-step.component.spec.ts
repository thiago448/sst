import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicInfosStepComponent } from './basic-infos-step.component';

describe('BasicInfosStepComponent', () => {
  let component: BasicInfosStepComponent;
  let fixture: ComponentFixture<BasicInfosStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicInfosStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInfosStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
