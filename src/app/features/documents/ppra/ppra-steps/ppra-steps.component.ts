import { SlidePanelComponent } from "./../../../../shared/slide-panel/slide-panel.component";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "smart-ppra-steps",
  templateUrl: "./ppra-steps.component.html",
  styleUrls: ["./ppra-steps.component.scss"]
})
export class PpraStepsComponent implements OnInit {
  @ViewChild(SlidePanelComponent, { static: false })
  slidePanel: SlidePanelComponent;

  collapseInfo = {
    isOpen: true,
    isValid: false
  };

  collapseRisks = {
    isOpen: true,
    isValid: false
  };

  collapseGeneralData = {
    isOpen: true,
    isValid: true
  };

  collapseScheduleActions = {
    isOpen: true,
    isValid: false
  };

  collapseFinalization = {
    isOpen: false,
    isValid: false
  };

  isCollapsed02 = true;
  isCollapsed03 = true;
  isCollapsed04 = true;

  formBasicInfo: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.formBasicInfo = this.formBuilder.group({
      name: ["Gabriel", Validators.required],
      lastName: ["Queiroz", Validators.required]
    });
  }

  next() {
    this.slidePanel.prevSlide();
  }
  prev() {
    this.slidePanel.nextSlide();
  }

  ngOnInit() {}
}
