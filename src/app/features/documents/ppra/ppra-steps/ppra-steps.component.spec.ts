import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PpraStepsComponent } from './ppra-steps.component';

describe('PpraStepsComponent', () => {
  let component: PpraStepsComponent;
  let fixture: ComponentFixture<PpraStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PpraStepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PpraStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
