import { Component, OnInit } from "@angular/core";
import { AngularEditorConfig } from "@kolkov/angular-editor";

@Component({
  selector: "smart-view-ppra",
  templateUrl: "./view-ppra.component.html",
  styleUrls: ["./view-ppra.component.scss"]
})
export class ViewPpraComponent implements OnInit {
  constructor() {}

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "500px",
    minHeight: "100%",
    maxHeight: "100%",
    width: "auto",
    minWidth: "0",
    translate: "yes",
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Editar seu PPRA",
    defaultParagraphSeparator: "",
    defaultFontName: "",
    defaultFontSize: "",
    fonts: [
      { class: "arial", name: "Arial" },
      { class: "times-new-roman", name: "Times New Roman" },
      { class: "calibri", name: "Calibri" },
      { class: "comic-sans-ms", name: "Comic Sans MS" }
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote"
      },
      {
        name: "redText",
        class: "redText"
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1"
      }
    ],
    uploadUrl: "v1/image",
    uploadWithCredentials: false,
    sanitize: true,
    outline: false,
    toolbarPosition: "top",
    toolbarHiddenButtons: [["bold", "italic"], ["fontSize"]]
  };

  htmlContent: any;

  ngOnInit(): void {}
}
