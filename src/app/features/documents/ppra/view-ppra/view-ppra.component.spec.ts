import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPpraComponent } from './view-ppra.component';

describe('ViewPpraComponent', () => {
  let component: ViewPpraComponent;
  let fixture: ComponentFixture<ViewPpraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPpraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPpraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
