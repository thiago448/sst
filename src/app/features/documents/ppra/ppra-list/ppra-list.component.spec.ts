import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PpraListComponent } from './ppra-list.component';

describe('PpraListComponent', () => {
  let component: PpraListComponent;
  let fixture: ComponentFixture<PpraListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PpraListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PpraListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
