import { Router } from "@angular/router";
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
} from "@angular/core";

@Component({
  selector: "smart-ppra-list",
  templateUrl: "./ppra-list.component.html",
  styleUrls: ["./ppra-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PpraListComponent implements OnInit {
  constructor(private router: Router) {}

  opened = true;

  isDropupButtonsNewFunction = false;

  @Input() activePane: PaneType = "left";

  ngOnInit() {}
  newPpra() {
    this.router.navigate(["documentos", "ppra", "novo"]);
  }
  toogle() {
    this.opened = this.opened = !this.opened;
  }
}
type PaneType = "left" | "right";
