import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalizationStepComponent } from './finalization-step.component';

describe('FinalizationStepComponent', () => {
  let component: FinalizationStepComponent;
  let fixture: ComponentFixture<FinalizationStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalizationStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalizationStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
