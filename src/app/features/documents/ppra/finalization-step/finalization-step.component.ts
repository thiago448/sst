import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "smart-finalization-step",
  templateUrl: "./finalization-step.component.html",
  styleUrls: ["./finalization-step.component.scss"]
})
export class FinalizationStepComponent implements OnInit {
  @Input()
  title = "Conclúido";

  @Input()
  status = "SUCCESS";

  constructor() {}

  ngOnInit() {}
}
