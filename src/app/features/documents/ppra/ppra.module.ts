import { FormNewGroupActivityModule } from "src/app/shared/form-new-group-activity/form-new-group-activity.module";
import { SlidePanelComponent } from "./register-risk-step/modals/control-epi-modal/slide-panel";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgxMaskModule } from "ngx-mask";
import { SharedModule } from "./../../../shared/shared.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { PPRARoutingModule } from "./ppra-routing.module";
import { PpraListComponent } from "./ppra-list/ppra-list.component";
import { ScheduleActionsStepComponent } from "./schedule-actions-step/schedule-actions-step.component";
import { PpraStepsComponent } from "./ppra-steps/ppra-steps.component";
import { RegisterRiskStepComponent } from "./register-risk-step/register-risk-step.component";
import { CollapseModule } from "ngx-bootstrap/collapse";
import { FinalizationStepComponent } from "./finalization-step/finalization-step.component";
import { BasicInfosStepComponent } from "./basic-infos-step/basic-infos-step.component";
import { MeteringStepComponent } from "./metering-step/metering-step.component";
import { TagsModule } from "src/app/shared/tags/tags.module";
import { RadioButtonModule } from "src/app/shared/radio-button/radio-button.module";
import { CheckboxModule } from "src/app/shared/checkbox/checkbox.module";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { FormNewFunctionModule } from "src/app/shared/form-new-function/form-new-function.module";
import { EmptyListModule } from "src/app/shared/empty-list/empty-list.module";
import { AngularEditorModule } from "@kolkov/angular-editor";
import { ViewPpraComponent } from "./view-ppra/view-ppra.component";
import { UtilsModule } from "src/app/shared/utils/utils.module";
import { SelectMultipleCheckboxModule } from "src/app/shared/select-multiple-checkbox/select-multiple-checkbox.module";
import { MeteringEquipmentModalComponent } from "./register-risk-step/modals/metering-equipment-modal/metering-equipment-modal.component";
import { RiskModalComponent } from "./register-risk-step/modals/risk-modal/risk-modal.component";
import { AddExistFunctionModalComponent } from "./register-risk-step/modals/add-exist-function-modal/add-exist-function-modal.component";
import { AddNewFunctionModalComponent } from "./register-risk-step/modals/add-new-function-modal/add-new-function-modal.component";
import { EmployeesListModalComponent } from "./register-risk-step/modals/employees-list-modal/employees-list-modal.component";
import { ControlEPIModalComponent } from "./register-risk-step/modals/control-epi-modal/control-epi-modal.component";
import { AddNewGroupActivityComponent } from "./register-risk-step/modals/add-new-group-activity/add-new-group-activity.component";
import { GheListModalComponent } from './register-risk-step/modals/ghe-list-modal/ghe-list-modal.component';
import { AddNewRiskModalComponent } from './register-risk-step/modals/add-new-risk-modal/add-new-risk-modal.component';

@NgModule({
  declarations: [
    PpraListComponent,
    ScheduleActionsStepComponent,
    PpraStepsComponent,
    RegisterRiskStepComponent,
    FinalizationStepComponent,
    BasicInfosStepComponent,
    MeteringStepComponent,
    ViewPpraComponent,
    MeteringEquipmentModalComponent,
    RiskModalComponent,
    AddExistFunctionModalComponent,
    AddNewFunctionModalComponent,
    EmployeesListModalComponent,
    ControlEPIModalComponent,
    SlidePanelComponent,
    AddNewGroupActivityComponent,
    GheListModalComponent,
    AddNewRiskModalComponent,
  ],
  imports: [
    CollapseModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CommonModule,
    PPRARoutingModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    TagsModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    FormNewFunctionModule,
    BsDropdownModule.forRoot(),
    EmptyListModule,
    AngularEditorModule,
    UtilsModule,
    SelectMultipleCheckboxModule,
    FormNewGroupActivityModule,
  ],
})
export class PPRAModule {}
