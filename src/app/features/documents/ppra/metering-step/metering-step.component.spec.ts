import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeteringStepComponent } from './metering-step.component';

describe('MeteringStepComponent', () => {
  let component: MeteringStepComponent;
  let fixture: ComponentFixture<MeteringStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeteringStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeteringStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
