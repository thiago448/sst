import { ModalModule } from "ngx-bootstrap/modal";
import { AsoRoutingModule } from "./aso-routing.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AsoListComponent } from "./aso-list/aso-list.component";
import { AsoFormComponent } from "./aso-form/aso-form.component";
import { SharedModule } from "src/app/shared/shared.module";
import { CheckboxModule } from "src/app/shared/checkbox/checkbox.module";
import { TagsModule } from "src/app/shared/tags/tags.module";
import { ButtonsModule } from "src/app/shared/buttons/buttons.module";
import { RadioButtonModule } from "src/app/shared/radio-button/radio-button.module";

@NgModule({
  declarations: [AsoListComponent, AsoFormComponent],
  imports: [
    CommonModule,
    AsoRoutingModule,
    SharedModule,
    CheckboxModule,
    RadioButtonModule,
    TagsModule,
    ButtonsModule,
    ModalModule,
  ],
})
export class AsoModule {}
