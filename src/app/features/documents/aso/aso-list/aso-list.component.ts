import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-aso-list",
  templateUrl: "./aso-list.component.html",
  styleUrls: ["./aso-list.component.scss"],
})
export class AsoListComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  newAso() {
    this.router.navigate(["documentos", "aso", "novo"]);
  }
}
