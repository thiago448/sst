import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AsoListComponent } from "./aso-list/aso-list.component";
import { AsoFormComponent } from "./aso-form/aso-form.component";
const routes: Routes = [
  {
    path: "",
    component: AsoListComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "aso",
          url: "/documentos/aso",
        },
      ],
    },
  },
  {
    path: "novo",
    component: AsoFormComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "aso",
          url: "/documentos/aso",
        },
        {
          title: "Novo",
          url: "/documentos/aso/novo",
        },
      ],
    },
  },
  {
    path: "editar/:id",
    component: AsoFormComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "aso",
          url: "/documentos/aso",
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsoRoutingModule {}
