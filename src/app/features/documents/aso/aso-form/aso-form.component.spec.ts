import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsoFormComponent } from './aso-form.component';

describe('AsoFormComponent', () => {
  let component: AsoFormComponent;
  let fixture: ComponentFixture<AsoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
