import { Component, OnInit, TemplateRef } from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

@Component({
  selector: "smart-aso-form",
  templateUrl: "./aso-form.component.html",
  styleUrls: ["./aso-form.component.scss"],
})
export class AsoFormComponent implements OnInit {
  modalRefAddMedic: BsModalRef;
  modalConfig = {
    keyboard: true,
    class: "modal-lg modal-dialog-centered",
  };

  constructor(private modalService: BsModalService) {}

  ngOnInit() {}

  openModalAddMedic(template: TemplateRef<any>) {
    this.modalRefAddMedic = this.modalService.show(template, this.modalConfig);
  }
}
