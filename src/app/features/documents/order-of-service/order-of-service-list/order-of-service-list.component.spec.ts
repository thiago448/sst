import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderOfServiceListComponent } from './order-of-service-list.component';

describe('OrderOfServiceListComponent', () => {
  let component: OrderOfServiceListComponent;
  let fixture: ComponentFixture<OrderOfServiceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderOfServiceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderOfServiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
