import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-order-of-service-list",
  templateUrl: "./order-of-service-list.component.html",
  styleUrls: ["./order-of-service-list.component.scss"],
})
export class OrderOfServiceListComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}
  newOrder() {
    this.router.navigate(["documentos", "ordem-servico", "novo"]);
  }
}
