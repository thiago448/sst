import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderOfServiceFormComponent } from './order-of-service-form.component';

describe('OrderOfServiceFormComponent', () => {
  let component: OrderOfServiceFormComponent;
  let fixture: ComponentFixture<OrderOfServiceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderOfServiceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderOfServiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
