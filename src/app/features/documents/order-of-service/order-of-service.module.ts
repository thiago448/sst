import { ButtonsModule } from "ngx-bootstrap/buttons";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { OrderOfServiceRoutingModule } from "./order-of-service-routing.module";
import { SharedModule } from "src/app/shared/shared.module";
import { CheckboxModule } from "src/app/shared/checkbox/checkbox.module";
import { TagsModule } from "src/app/shared/tags/tags.module";
import { OrderOfServiceFormComponent } from "./order-of-service-form/order-of-service-form.component";
import { OrderOfServiceListComponent } from "./order-of-service-list/order-of-service-list.component";

@NgModule({
  declarations: [OrderOfServiceFormComponent, OrderOfServiceListComponent],
  imports: [
    CommonModule,
    OrderOfServiceRoutingModule,
    SharedModule,
    CheckboxModule,
    TagsModule,
    ButtonsModule
  ]
})
export class OrderOfServiceModule {}
