import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { OrderOfServiceFormComponent } from "./order-of-service-form/order-of-service-form.component";
import { OrderOfServiceListComponent } from "./order-of-service-list/order-of-service-list.component";
const routes: Routes = [
  {
    path: "",
    component: OrderOfServiceListComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "Ordem de Serviços",
          url: "/documentos/ordem-servico",
        },
      ],
    },
  },
  {
    path: "novo",
    component: OrderOfServiceFormComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "Ordem de Serviços",
          url: "/documentos/ordem-servico",
        },
        {
          title: "Nova Ordem de Serviços",
          url: "/documentos/ordem-servico/novo",
        },
      ],
    },
  },
  {
    path: "editar/:id",
    component: OrderOfServiceFormComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "Ordem de Serviços",
          url: "/documentos/ordem-servico",
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderOfServiceRoutingModule {}
