import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TesteComponent } from "./teste/teste.component";

const routes: Routes = [
  {
    path: "ppra",
    loadChildren: () => import("./ppra/ppra.module").then((m) => m.PPRAModule),
  },
  {
    path: "ordem-servico",
    loadChildren: () =>
      import("./order-of-service/order-of-service.module").then(
        (m) => m.OrderOfServiceModule
      ),
  },
  {
    path: "pcmso",
    loadChildren: () =>
      import("./pcmso/pcmso.module").then((m) => m.PCMSOModule),
  },
  {
    path: "aso",
    loadChildren: () => import("./aso/aso.module").then((m) => m.AsoModule),
  },
  {
    path: "teste",
    component: TesteComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentsRoutingModule {}
