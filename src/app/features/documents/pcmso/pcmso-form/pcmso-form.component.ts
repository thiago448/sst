import { Component, OnInit, TemplateRef } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";

@Component({
  selector: "smart-pcmso-form",
  templateUrl: "./pcmso-form.component.html",
  styleUrls: ["./pcmso-form.component.scss"],
})
export class PcmsoFormComponent implements OnInit {
  modalRefNewRisk: BsModalRef;
  modalRefNewExam: BsModalRef;
  modalConfig = {
    keyboard: true,
    class: "modal-lg   modal-dialog-centered",
  };
  visibleNewExam = false;
  listPeriodsNewExams = [];
  exams = [
    {
      name: "Audiometria",
      periods: [{}, {}, {}],
    },
    {
      name: "Espirometria",
      periods: [{}, {}, {}],
    },
    {
      name: "Audiometria",
      periods: [{}, {}, {}],
    },
    {
      name: "Espirometria",
      periods: [{}, {}, {}],
    },
  ];

  constructor(private modalService: BsModalService) {}

  ngOnInit() {}

  openModalRisk(template: TemplateRef<any>) {
    this.modalRefNewRisk = this.modalService.show(template, this.modalConfig);
  }
  openModalExam(template: TemplateRef<any>) {
    this.modalRefNewExam = this.modalService.show(template, this.modalConfig);
  }
  addPeriod(item) {
    this.listPeriodsNewExams.push(item);
  }
  removePeriod(item) {
    const index = this.listPeriodsNewExams.indexOf(item);
    if (index > -1) {
      this.listPeriodsNewExams.splice(index, 1);
    }
  }

  newExam() {
    this.visibleNewExam = true;
  }
  closeCardExam() {
    this.visibleNewExam = false;
  }
}
