import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcmsoFormComponent } from './pcmso-form.component';

describe('PcmsoFormComponent', () => {
  let component: PcmsoFormComponent;
  let fixture: ComponentFixture<PcmsoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcmsoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcmsoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
