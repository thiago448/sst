import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "smart-pcmso-exam-item",
  templateUrl: "./pcmso-exam-item.component.html",
  styleUrls: ["./pcmso-exam-item.component.scss"],
})
export class PcmsoExamItemComponent implements OnInit {
  @Input()
  exam;

  showDetails = false;

  constructor() {}

  ngOnInit() {}
  onBlur() {
    this.showDetails = false;
  }
  onClickMenu(event: Event) {
    event.stopPropagation();
  }
}
