import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcmsoExamItemComponent } from './pcmso-exam-item.component';

describe('PcmsoExamItemComponent', () => {
  let component: PcmsoExamItemComponent;
  let fixture: ComponentFixture<PcmsoExamItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcmsoExamItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcmsoExamItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
