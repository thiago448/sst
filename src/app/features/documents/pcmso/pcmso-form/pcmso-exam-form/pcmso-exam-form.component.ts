import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "smart-pcmso-exam-form",
  templateUrl: "./pcmso-exam-form.component.html",
  styleUrls: ["./pcmso-exam-form.component.scss"],
})
export class PcmsoExamFormComponent implements OnInit {
  @Input()
  visibleCardFormExam = false;

  periods = [];

  @Output()
  onCloseCardFormExam = new EventEmitter<any>();

  @Output()
  onCreateExam = new EventEmitter<any>();

  visibleCardListExams = false;

  @Input()
  listPeriodsNewExams = [];

  exams = [
    {
      name: "Audiometria",
      periods: [{}, {}, {}],
    },
    {
      name: "Espirometria",
      periods: [{}, {}, {}],
    },
    {
      name: "Audiometria",
      periods: [{}, {}, {}],
    },
    {
      name: "Espirometria",
      periods: [{}, {}, {}],
    },
  ];
  constructor() {}

  ngOnInit() {}

  saveExam() {
    this.onCreateExam.emit();
  }

  cancelCreateExam() {
    this.onCloseCardFormExam.emit();
  }

  showCardListExams() {
    this.visibleCardListExams = true;
  }
  closeCardListExams() {
    this.visibleCardListExams = false;
  }
  addPeriod(period) {
    this.listPeriodsNewExams.push(period);
  }

  removePeriod(period) {
    const index = this.listPeriodsNewExams.indexOf(period);
    if (index > -1) {
      this.listPeriodsNewExams.splice(index, 1);
    }
  }
}
