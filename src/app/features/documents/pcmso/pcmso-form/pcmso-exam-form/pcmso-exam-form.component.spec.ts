import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcmsoExamFormComponent } from './pcmso-exam-form.component';

describe('PcmsoExamFormComponent', () => {
  let component: PcmsoExamFormComponent;
  let fixture: ComponentFixture<PcmsoExamFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcmsoExamFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcmsoExamFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
