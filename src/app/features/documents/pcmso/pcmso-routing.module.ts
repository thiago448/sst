import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PcmsoListComponent } from "./pcmso-list/pcmso-list.component";
import { PcmsoFormComponent } from "./pcmso-form/pcmso-form.component";

const routes: Routes = [
  {
    path: "",
    component: PcmsoListComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "pcmso's",
          url: "/documentos/pcmso",
        },
      ],
    },
  },
  {
    path: "novo",
    component: PcmsoFormComponent,
    data: {
      breadcrumbs: [
        {
          title: "Documentos",
          url: "/documentos",
        },
        {
          title: "pcmso's",
          url: "/documentos/pcmso/novo",
        },
      ],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PCMSORoutingModule {}
