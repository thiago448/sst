import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcmsoListComponent } from './pcmso-list.component';

describe('PcmsoListComponent', () => {
  let component: PcmsoListComponent;
  let fixture: ComponentFixture<PcmsoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcmsoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcmsoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
