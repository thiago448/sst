import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-pcmso-list",
  templateUrl: "./pcmso-list.component.html",
  styleUrls: ["./pcmso-list.component.scss"],
})
export class PcmsoListComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  newPcmso() {
    this.router.navigate(["documentos", "pcmso", "novo"]);
  }
}
