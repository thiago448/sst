import { ModalModule } from "ngx-bootstrap/modal";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { RadioButtonModule } from "src/app/shared/radio-button/radio-button.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PCMSORoutingModule } from "./pcmso-routing.module";
import { PcmsoListComponent } from "./pcmso-list/pcmso-list.component";
import { PcmsoFormComponent } from "./pcmso-form/pcmso-form.component";
import { SharedModule } from "src/app/shared/shared.module";
import { SelectMultipleCheckboxModule } from "src/app/shared/select-multiple-checkbox/select-multiple-checkbox.module";
import { PcmsoExamFormComponent } from "./pcmso-form/pcmso-exam-form/pcmso-exam-form.component";
import { PcmsoExamItemComponent } from "./pcmso-form/pcmso-exam-item/pcmso-exam-item.component";

@NgModule({
  declarations: [
    PcmsoListComponent,
    PcmsoFormComponent,
    PcmsoExamFormComponent,
    PcmsoExamItemComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    PCMSORoutingModule,
    RadioButtonModule,
    SelectMultipleCheckboxModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
  ],
})
export class PCMSOModule {}
