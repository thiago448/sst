import { CheckboxModule } from "src/app/shared/checkbox/checkbox.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SignInRoutingModule } from "./sign-in-routing.module";
import { SignInComponent } from "./sign-in.component";
import { SharedModule } from "src/app/shared/shared.module";
import { AlertModule } from "ngx-bootstrap/alert";

@NgModule({
  declarations: [SignInComponent],
  imports: [
    CommonModule,
    SignInRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AlertModule.forRoot(),
    CheckboxModule,
  ],
})
export class SignInModule {}
