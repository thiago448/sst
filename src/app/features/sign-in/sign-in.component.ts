import { HttpErrorResponse } from "@angular/common/http";
import { TokenModel } from "./../../core/models/token-model";
import { Store, createSelector } from "@ngrx/store";
import { Observable, throwError } from "rxjs";
import { Router } from "@angular/router";
import { AuthModel } from "./../../core/models/auth-model";
import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from "@angular/forms";
import { AuthService } from "src/app/core/services/auth.service";
import { selectUserState } from "src/app/store/user";
import { UsuarioModel } from "src/app/core/models/usuario-model";
import { AlertComponent } from "ngx-bootstrap/alert/alert.component";
import { CepService } from "src/app/core/services/cep.service";
@Component({
  selector: "smart-sign-in",
  templateUrl: "./sign-in.component.html",
  styleUrls: ["./sign-in.component.scss"],
})
export class SignInComponent implements OnInit {
  private readonly USER_VALIDATION_REGEX: RegExp = new RegExp(
    "^[a-z0-9._%+-]+@[a-z0-9.-]"
  );
  public form: FormGroup;
  public submitted = false;
  public alerts: any[] = [];

  public user: Observable<UsuarioModel> = this.store.select(
    createSelector(selectUserState, (user) => user)
  );
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private store: Store<any>,
    private cepService: CepService
  ) {
    this.form = this.formBuilder.group({
      username: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(this.USER_VALIDATION_REGEX),
        ]),
      ],
      password: ["", Validators.compose([Validators.required])],
    });
  }
  get f() {
    return this.form.controls;
  }

  formSubmit() {
    if (!this.form.valid) {
      return this.validateAllFormFields(this.form);
    }
    const authModel: AuthModel = new AuthModel();
    authModel.username = this.form.value.username;
    authModel.password = this.form.value.password;
    this.authService.login(authModel).subscribe(
      (tokenResponse) => {
        let token: TokenModel = new TokenModel();
        token = Object.assign(token, tokenResponse);
        this.authService.updateToken(token.access_token);
        this.authService.updateRefreshToken(token.refresh_token);
        this.router.navigate(["/"]);
      },
      (error: HttpErrorResponse) => {
        console.log(error.status);
        this.alerts.push({
          type: "danger",
          msg: `Usuário inexistente ou senha inválida`,
          // timeout: 20000,
        });
      }
    );
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsPristine({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter((alert) => alert !== dismissedAlert);
  }

  ngOnInit() {
    this.cepService.getCEP("58220000").subscribe((data) => {
      console.log(data);
    });
  }
}
