import {
  Router,
  RouterEvent,
  RouteConfigLoadStart,
  RouteConfigLoadEnd,
} from "@angular/router";
import { Component, NgZone } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { AnimationOptions } from "ngx-lottie";
import { AnimationItem } from "lottie-web";

@Component({
  selector: "smart-root",
  templateUrl: "./app.component.html",
})
export class AppComponent {
  constructor(
    private ngZone: NgZone,
    router: Router,
    busyService: NgxSpinnerService
  ) {
    router.events.subscribe((event: RouterEvent) => {
      if (event instanceof RouteConfigLoadStart) {
        busyService.show();
      } else if (event instanceof RouteConfigLoadEnd) {
        setTimeout(() => {
          busyService.hide();
        }, 1200);
      }
    });
  }
  options: AnimationOptions = {
    path: "/assets/lottie/loader.json",
    loop: true,
    renderer: "canvas",
    autoplay: true,
  };

  title = "smartadmin-angular-seed";
}
