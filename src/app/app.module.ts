import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CoreModule } from "./core/core.module";
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";
import { ToastComponent } from "./shared/toast/toast.component";
import { LottieModule } from "ngx-lottie";

export function playerFactory() {
  return import("lottie-web");
}

@NgModule({
  declarations: [AppComponent, ToastComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CoreModule,
    NgxSpinnerModule,
    LottieModule.forRoot({ player: playerFactory, useCache: true }),
    ToastrModule.forRoot({
      toastComponent: ToastComponent,
      timeOut: 5000,
      iconClasses: {
        success: "toast__success",
        warning: "toast__warning",
        error: "toast__error",
        info: "toast__info",
      },
    }),
  ],
  entryComponents: [ToastComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
