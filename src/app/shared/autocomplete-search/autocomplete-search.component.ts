import { debounceTime } from "rxjs/operators";
import { distinctUntilChanged } from "rxjs/operators";
import { FormGroup, FormBuilder } from "@angular/forms";
import {
  Component,
  OnInit,
  HostListener,
  EventEmitter,
  Output,
  Input,
} from "@angular/core";

@Component({
  selector: "smart-autocomplete-search",
  templateUrl: "./autocomplete-search.component.html",
  styleUrls: ["./autocomplete-search.component.scss"],
})
export class AutocompleteSearchComponent implements OnInit {
  public form: FormGroup;

  public isOpen: boolean = false;

  @Input() public labelName: string;

  @Input() public id: string;

  @Input() public isBig: false;

  @Input() public placeholderName: string;

  @Input() public showLabel: true;

  public clickedInside: boolean = false;

  @Output() public clickEvent: EventEmitter<any> = new EventEmitter();

  public isSearch = true;

  @Input()
  public items: Array<any> = [
    {
      id: 1,
      name: "option 1",
    },
    {
      id: 2,
      name: "option 2",
    },
    {
      id: 3,
      name: "option 3",
    },
    {
      id: 4,
      name: "option 4",
    },
    {
      id: 5,
      name: "option 5",
    },
  ];

  public itemsResult: Array<any> = [];

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      search: [""],
    });
  }

  ngOnInit() {
    this.form
      .get("search")
      .valueChanges.pipe(distinctUntilChanged())
      .subscribe((result) => {
        this.itemsResult = [];
        this.isOpen = false;
        const filtered = this.items.filter((item) =>
          item.name.includes(result.length > 0 ? result : null)
        );
        if (filtered.length > 0) {
          this.isOpen = true;
          this.itemsResult = filtered;
        }
      });
  }

  @HostListener("click")
  public clickInsideComponent() {
    this.clickedInside = true;
  }

  @HostListener("document:click")
  public clickOutsideComponent() {
    if (!this.clickedInside) {
      this.isOpen = false;
      if (!this.items.find((item) => item.name === this.form.value.search))
        this.form.setValue({ search: "" });
    }
    this.clickedInside = false;
  }
  selectItem(item) {
    this.form.setValue({ search: item.name });
    this.clickEvent.emit(item);
    this.isOpen = false;
  }
  isInputReadOnly() {
    return !this.isSearch;
  }
  autocompleteChange(e) {}
}
