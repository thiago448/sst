import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AutocompleteSearchComponent } from "./autocomplete-search.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [AutocompleteSearchComponent],
  imports: [CommonModule, ReactiveFormsModule, FormsModule],
  exports: [AutocompleteSearchComponent],
})
export class AutoCompleteSearchModule {}
