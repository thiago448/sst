import { ButtonsModule } from "../buttons/buttons.module";
import { EmptyListComponent } from "./empty-list.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LottieModule } from "ngx-lottie";

export function playerFactory() {
  return import("lottie-web");
}

@NgModule({
  declarations: [EmptyListComponent],
  imports: [
    CommonModule,
    ButtonsModule,
    LottieModule.forRoot({ player: playerFactory })
  ],
  exports: [EmptyListComponent]
})
export class EmptyListModule {}
