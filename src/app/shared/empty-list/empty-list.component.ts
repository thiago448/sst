import {
  Component,
  OnInit,
  NgZone,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { AnimationOptions } from "ngx-lottie";
import { AnimationItem } from "lottie-web";

@Component({
  selector: "smart-empty-list",
  templateUrl: "./empty-list.component.html",
  styleUrls: ["./empty-list.component.scss"]
})
export class EmptyListComponent implements OnInit {
  options: AnimationOptions = {
    path: "/assets/lottie/empty-box.json"
  };

  private animationItem: AnimationItem;

  @Input()
  buttonTitle: string;

  @Output()
  clickButtonEvent = new EventEmitter<any>();

  ngOnInit() {}

  constructor(private ngZone: NgZone) {}

  animationCreated(animationItem: AnimationItem): void {
    this.animationItem = animationItem;
  }

  stop(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.stop());
  }

  play(): void {
    this.ngZone.runOutsideAngular(() => this.animationItem.play());
  }
  clickButtonPlus() {
    this.clickButtonEvent.emit();
  }
}
