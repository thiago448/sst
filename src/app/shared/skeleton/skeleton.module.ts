import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CardSkeletonComponent } from "./card-skeleton/card-skeleton.component";
import { TableSkeletonComponent } from "./table-skeleton/table-skeleton.component";
import { NgxSkeletonLoaderModule } from "ngx-skeleton-loader";

@NgModule({
  declarations: [CardSkeletonComponent, TableSkeletonComponent],
  imports: [CommonModule, NgxSkeletonLoaderModule],
  exports: [CardSkeletonComponent, TableSkeletonComponent],
})
export class SkeletonModule {}
