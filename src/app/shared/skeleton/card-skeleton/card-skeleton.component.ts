import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-card-skeleton",
  templateUrl: "./card-skeleton.component.html",
  styleUrls: ["./card-skeleton.component.scss"],
})
export class CardSkeletonComponent implements OnInit {
  configBgSkeleton = {
    "background-color": "#e2e2e2",
  };

  configAvatar = {
    ...this.configBgSkeleton,
    height: "80px",
    width: "80px",
  };

  configLineOne = {
    ...this.configBgSkeleton,
    "border-radius": "20px",
    margin: "10px 10%",
    width: "80%",
  };
  configLineTwo = {
    ...this.configBgSkeleton,
    "border-radius": "20px",
    margin: "0px 5%",
    width: "90%",
  };

  constructor() {}

  ngOnInit() {}
}
