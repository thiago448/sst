import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerFormWithTabsComponent } from './container-form-with-tabs.component';

describe('ContainerFormWithTabsComponent', () => {
  let component: ContainerFormWithTabsComponent;
  let fixture: ComponentFixture<ContainerFormWithTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerFormWithTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerFormWithTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
