import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectConContainerComponent } from './connect-con-container.component';

describe('ConnectConContainerComponent', () => {
  let component: ConnectConContainerComponent;
  let fixture: ComponentFixture<ConnectConContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectConContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectConContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
