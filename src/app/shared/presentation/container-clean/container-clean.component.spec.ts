import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerCleanComponent } from './container-clean.component';

describe('ContainerCleanComponent', () => {
  let component: ContainerCleanComponent;
  let fixture: ComponentFixture<ContainerCleanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerCleanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerCleanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
