import { ModalDirective } from "ngx-bootstrap/modal";
import { Component, OnInit, ViewChild, Input } from "@angular/core";

@Component({
  selector: "smart-feedback-modal",
  templateUrl: "./feedback-modal.component.html",
  styleUrls: ["./feedback-modal.component.scss"],
})
export class FeedbackModalComponent implements OnInit {
  @ViewChild("autoShownModal", { static: false })
  autoShownModal: ModalDirective;

  showSuccess = true;

  @Input()
  isModalShown = false;

  @Input()
  title: string;

  @Input() buttonText = "Confirmar";

  showModal(showSuccess: boolean = true): void {
    this.showSuccess = showSuccess;
    this.isModalShown = true;
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

  onHidden(): void {
    this.isModalShown = false;
  }

  constructor() {}

  ngOnInit(): void {}
}
