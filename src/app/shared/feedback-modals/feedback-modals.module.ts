import { ModalModule } from "ngx-bootstrap/modal";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FeedbackModalComponent } from "./feedback-modal/feedback-modal.component";

@NgModule({
  declarations: [FeedbackModalComponent],
  imports: [CommonModule, ModalModule.forRoot()],
  exports: [FeedbackModalComponent],
})
export class FeedbackModalsModule {}
