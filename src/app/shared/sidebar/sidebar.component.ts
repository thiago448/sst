import { APP_CONFIG } from "src/app/app.config";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "smart-sidebar-nav",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"]
})
export class SidebarComponent implements OnInit {
  logo = APP_CONFIG.logo;
  appName = APP_CONFIG.appName;
  constructor() {}

  ngOnInit() {}
}
