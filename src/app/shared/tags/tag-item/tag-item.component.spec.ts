import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TagItemComponent } from "./tag-item.component";

describe("TagItemComponent", () => {
  let component: TagItemComponent;
  let fixture: ComponentFixture<TagItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TagItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture.detectChanges();
    fixture = TestBed.createComponent(TagItemComponent);
    component = fixture.componentInstance;
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
