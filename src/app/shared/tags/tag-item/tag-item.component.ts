import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "smart-tag-item",
  templateUrl: "./tag-item.component.html",
  styleUrls: ["./tag-item.component.scss"]
})
export class TagItemComponent implements OnInit {
  @Input() title = "title";

  @Input() isFavorite = false;

  @Input() showEditButton = true;

  @Input() showDeleteButton = true;

  @Output() clickTagEvent = new EventEmitter<any>();

  @Output() clickEditEvent = new EventEmitter<any>();

  @Output() clickDeleteEvent = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  clickDelete(event: Event) {
    event.stopPropagation();
    this.clickDeleteEvent.emit();
  }
  clickEdit(event: Event) {
    event.stopPropagation();

    this.clickEditEvent.emit();
  }
  clickTag() {
    this.clickTagEvent.emit();
  }
}
