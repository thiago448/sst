import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TagItemComponent } from "./tag-item/tag-item.component";
import { TagContainerComponent } from "./tag-container/tag-container.component";
import { UtilsModule } from "../utils/utils.module";
@NgModule({
  declarations: [TagItemComponent, TagContainerComponent],
  imports: [CommonModule, UtilsModule],
  exports: [TagItemComponent, TagContainerComponent]
})
export class TagsModule {}
