import { FormFieldErrorComponent } from "./form-field-error/form-field-error.component";
import {
  Directive,
  ElementRef,
  OnInit,
  Input,
  Renderer2,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentRef,
  ChangeDetectionStrategy
} from "@angular/core";
import { NgControl, FormControl } from "@angular/forms";

@Directive({
  selector: "[formField]"
})
export class FormFieldDirective implements OnInit {
  private ref: ComponentRef<FormFieldErrorComponent>;
  private container: ViewContainerRef;
  private eventRegistered = false;

  constructor(
    private vcr: ViewContainerRef,
    private el: ElementRef,
    private control: NgControl,
    private renderer: Renderer2,
    private resolver: ComponentFactoryResolver
  ) {
    this.container = vcr;
  }

  @Input("formField")
  set formField(value) {
    if (!this.eventRegistered) {
      this.updateData(value);
    }

    this.control.valueChanges.subscribe(() => {
      this.updateData(!this.control.valid && this.control.touched);
      this.eventRegistered = true;
    });
  }

  updateData(value) {
    if (value) {
      if (this.control.hasError) {
        //this.renderer.addClass(this.el.nativeElement, "warning-info");
        // this.renderer.removeClass(this.el.nativeElement, "warning-success");
      }
    } else {
      //this.renderer.removeClass(this.el.nativeElement, "warning-info");
      //this.renderer.addClass(this.el.nativeElement, "warning-success");
    }
    // else{
    //   this.renderer.removeClass(this.el.nativeElement, "warning-success");
    // }

    this.showErrors();
    this.ref.instance.displayError =
      !this.control.valid && this.control.touched;
    this.ref.instance.errorsField = this.control.errors;
  }

  showErrors() {
    if (!this.ref) {
      const factory = this.resolver.resolveComponentFactory(
        FormFieldErrorComponent
      );
      this.ref = this.container.createComponent(factory);
      this.ref.instance.displayError =
        !this.control.valid && this.control.touched;
      this.ref.instance.errorsField = this.control.errors;
    }
  }

  ngOnInit() {
    // this.control.valueChanges.subscribe(value => {
    //   console.log("mudou");
    // });
  }
}
