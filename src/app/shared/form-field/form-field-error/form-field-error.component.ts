import {
  Component,
  OnInit,
  Input,
  Inject,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from "@angular/core";
import { FORM_ERRORS } from "../form-field-erros";
@Component({
  selector: "smart-form-field-error",
  templateUrl: "./form-field-error.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ["./form-field-error.component.scss"]
})
export class FormFieldErrorComponent implements OnInit {
  _error;
  _message;

  constructor(
    @Inject(FORM_ERRORS) private errors,
    private cdr: ChangeDetectorRef
  ) {}

  icons = {
    success: "assets/img/icons/icon-form-check-right.svg",
    warn: "assets/img/icons/icon-form-warn.svg",
    error: "assets/img/icons/icon-form-error.svg"
  };

  ngOnInit() {}

  @Input() errorsMessage: object = {};

  @Input() set displayError(value) {
    this._error = value;
    this.cdr.detectChanges();
  }

  @Input() set errorsField(errors) {
    this.cdr.detectChanges();
    const firstKey = Object.keys(errors || {})[0];
    if (firstKey) {
      const message = this.errors[firstKey]();
      this._message = message;
      this.cdr.detectChanges();
      return;
    }
    this._message = "";
    this.cdr.detectChanges();
    return;
  }
}
