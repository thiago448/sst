export enum FINISH_STATUS {
  SUCCESS,
  ALERT
}

export class ConfigFormField {
  errorsMessage: Array<any>;
  status: FINISH_STATUS;
  isFieldValid: boolean;

  constructor() {
    this.status = FINISH_STATUS.SUCCESS;
  }
}
