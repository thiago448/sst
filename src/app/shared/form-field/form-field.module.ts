import { FormFieldErrorComponent } from "./form-field-error/form-field-error.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormFieldDirective } from "./form-field.directive";
import { RequireIfDirective } from './require-if.directive';

@NgModule({
  declarations: [FormFieldErrorComponent, FormFieldDirective, RequireIfDirective],
  imports: [CommonModule],
  entryComponents: [FormFieldErrorComponent],
  exports: [FormFieldErrorComponent, FormFieldDirective]
})
export class FormFieldModule {}
