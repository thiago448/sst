import { FormGroup, FormBuilder } from "@angular/forms";
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener
} from "@angular/core";

@Component({
  selector: "smart-select-multiple-checkbox",
  templateUrl: "./select-multiple-checkbox.component.html",
  styleUrls: ["./select-multiple-checkbox.component.scss"]
})
export class SelectMultipleCheckboxComponent implements OnInit {
  public form: FormGroup;

  public isOpen: boolean = false;

  @Input() public labelName: string = null;

  @Input() public placeholderName: string;

  public clickedInside: boolean = false;

  @Output() public clickEvent: EventEmitter<any> = new EventEmitter();

  @Input()
  public isSearch = false;

  @Input()
  public items: Array<any> = [
    {
      id: 1,
      name: "option 1"
    },
    {
      id: 2,
      name: "option 2"
    },
    {
      id: 3,
      name: "option 3"
    },
    {
      id: 4,
      name: "option 4"
    },
    {
      id: 5,
      name: "option 5"
    }
  ];

  public itemsResult: Array<any> = [];

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      search: [""]
    });
  }

  ngOnInit() {
    this.form.get("search").valueChanges.subscribe(result => {
      this.itemsResult = [];
      const filtered = this.items.filter(item =>
        item.name.includes(result.length > 0 ? result : null)
      );
      if (filtered.length > 0) {
        this.isOpen = true;
        this.itemsResult = filtered;
      }
    });
  }

  @HostListener("click")
  public clickInsideComponent() {
    this.clickedInside = true;
  }

  @HostListener("document:click")
  public clickOutsideComponent() {
    if (!this.clickedInside) {
      this.isOpen = false;
      if (
        !this.items.find(item => item.name === this.form.value.search) &&
        this.isSearch
      )
        this.form.setValue({ search: "" });
    }
    this.clickedInside = false;
  }

  ToggleOpenDropDown() {
    if (!this.isSearch) {
      this.isOpen = !this.isOpen;
    }
  }
  selectItem(item) {
    this.form.setValue({ search: item.name });
    this.clickEvent.emit(item);
    this.isOpen = false;
  }
  isInputReadOnly() {
    return !this.isSearch;
  }
}
