import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { SelectMultipleCheckboxComponent } from "./select-multiple-checkbox.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CheckboxModule } from "../checkbox/checkbox.module";

@NgModule({
  declarations: [SelectMultipleCheckboxComponent],
  imports: [CommonModule, CheckboxModule, ReactiveFormsModule, FormsModule],
  exports: [SelectMultipleCheckboxComponent]
})
export class SelectMultipleCheckboxModule {}
