import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMultipleCheckboxComponent } from './select-multiple-checkbox.component';

describe('SelectMultipleCheckboxComponent', () => {
  let component: SelectMultipleCheckboxComponent;
  let fixture: ComponentFixture<SelectMultipleCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMultipleCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMultipleCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
