import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "smart-button-remove",
  templateUrl: "./button-remove.component.html",
  styleUrls: ["./button-remove.component.scss"]
})
export class ButtonRemoveComponent implements OnInit {
  @Input()
  title: string;

  @Input()
  iconTrash: boolean = false;

  @Output()
  click = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  onClick() {
    this.click.emit();
  }
}
