import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "smart-buttons-form",
  templateUrl: "./buttons-form.component.html",
  styleUrls: ["./buttons-form.component.scss"]
})
export class ButtonsFormComponent implements OnInit {
  @Input()
  cancelButtonTitle = "Cancelar";

  @Output()
  cancelButtonClick = new EventEmitter<any>();

  @Input()
  saveButtonTitle = "Salvar";

  @Output()
  saveButtonClick = new EventEmitter<any>();

  @Input()
  saveAndContinueButtonTitle = "Salvar e criar um novo";

  @Output()
  saveAndContinueButtonClick = new EventEmitter<any>();

  @Input()
  showSaveAndContinueButton = true;

  constructor() {}

  ngOnInit() {}
}
