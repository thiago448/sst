import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "smart-button-add",
  templateUrl: "./button-add.component.html",
  styleUrls: ["./button-add.component.scss"]
})
export class ButtonAddComponent implements OnInit {
  @Input()
  title: string;

  @Input()
  withBorder = false;

  @Output()
  click = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  onClick() {
    // this.click.emit();
  }
}
