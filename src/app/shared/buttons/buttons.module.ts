import { ButtonRemoveComponent } from "./button-remove/button-remove.component";
import { ButtonsFormComponent } from "./buttons-form/buttons-form.component";
import { ButtonAddComponent } from "./button-add/button-add.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [
    ButtonAddComponent,
    ButtonsFormComponent,
    ButtonRemoveComponent
  ],
  imports: [CommonModule],
  exports: [ButtonAddComponent, ButtonsFormComponent, ButtonRemoveComponent]
})
export class ButtonsModule {}
