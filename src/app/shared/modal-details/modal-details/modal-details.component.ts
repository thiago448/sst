import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";

@Component({
  selector: "smart-modal-details",
  templateUrl: "./modal-details.component.html",
  styleUrls: ["./modal-details.component.scss"],
})
export class ModalDetailsComponent implements OnInit {
  @ViewChild("autoShownModal", { static: false })
  autoShownModal: ModalDirective;

  @Input()
  isModalShown = false;

  @Input()
  title: string;

  @Input() principalText: string;

  showModal(): void {
    console.log("entrou");
    this.isModalShown = true;
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

  onHidden(): void {
    this.isModalShown = false;
  }

  constructor() {}

  ngOnInit(): void {}
}
