import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "smart-modal-detail-item",
  templateUrl: "./modal-detail-item.component.html",
  styleUrls: ["./modal-detail-item.component.scss"]
})
export class ModalDetailItemComponent implements OnInit {
  constructor() {}

  @Input() label: string;

  @Input() value: string;

  ngOnInit(): void {}
}
