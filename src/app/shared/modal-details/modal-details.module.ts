import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ModalDetailsComponent } from "./modal-details/modal-details.component";
import { ModalModule } from "ngx-bootstrap/modal";
import { ModalDetailItemComponent } from "./modal-detail-item/modal-detail-item.component";

@NgModule({
  declarations: [ModalDetailsComponent, ModalDetailItemComponent],
  imports: [CommonModule, ModalModule.forRoot()],
  exports: [ModalDetailsComponent, ModalDetailItemComponent]
})
export class ModalDetailsModule {}
