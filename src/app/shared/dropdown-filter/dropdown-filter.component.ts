import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "smart-dropdown-filter",
  templateUrl: "./dropdown-filter.component.html",
  styleUrls: ["./dropdown-filter.component.scss"],
})
export class DropdownFilterComponent implements OnInit {
  constructor() {}

  @Input()
  title;

  @Input()
  filterHeader;

  ngOnInit() {}
}
