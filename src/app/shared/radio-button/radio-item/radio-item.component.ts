import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "smart-radio-item",
  templateUrl: "./radio-item.component.html",
  styleUrls: ["./radio-item.component.scss"],
})
export class RadioItemComponent implements OnInit {
  constructor() {}
  @Input() title = "abc";
  ngOnInit() {}
  @Input() black = false;
}
