import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "smart-radio-container",
  templateUrl: "./radio-container.component.html",
  styleUrls: ["./radio-container.component.scss"],
})
export class RadioContainerComponent implements OnInit {
  @Input() label = "Radio title";

  @Input() direction = "column";

  @Input() center = false;

  @Input() black = false;

  constructor() {}

  ngOnInit() {
    console.log(this.center);
  }
}
