import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RadioItemComponent } from "./radio-item/radio-item.component";
import { RadioContainerComponent } from "./radio-item/radio-container/radio-container.component";

@NgModule({
  declarations: [RadioItemComponent, RadioContainerComponent],
  imports: [CommonModule],
  exports: [RadioItemComponent, RadioContainerComponent]
})
export class RadioButtonModule {}
