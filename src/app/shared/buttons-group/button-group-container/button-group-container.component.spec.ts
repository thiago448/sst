import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonGroupContainerComponent } from './button-group-container.component';

describe('ButtonGroupContainerComponent', () => {
  let component: ButtonGroupContainerComponent;
  let fixture: ComponentFixture<ButtonGroupContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonGroupContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonGroupContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
