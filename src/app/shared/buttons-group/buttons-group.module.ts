import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ButtonGroupContainerComponent } from "./button-group-container/button-group-container.component";
import { ButtonGroupItemComponent } from "./button-group-item/button-group-item.component";

@NgModule({
  declarations: [ButtonGroupContainerComponent, ButtonGroupItemComponent],
  imports: [CommonModule],
  exports: [ButtonGroupItemComponent]
})
export class ButtonsGroupModule {}
