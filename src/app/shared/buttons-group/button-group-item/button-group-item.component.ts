import { Component, OnInit, Input } from "@angular/core";

export enum Position {
  left,
  middle,
  right
}

@Component({
  selector: "smart-button-group-item",
  templateUrl: "./button-group-item.component.html",
  styleUrls: ["./button-group-item.component.scss"]
})
export class ButtonGroupItemComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
