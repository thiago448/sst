import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { ModalDirective } from "ngx-bootstrap/modal";

@Component({
  selector: "smart-modal-delete",
  templateUrl: "./modal-delete.component.html",
  styleUrls: ["./modal-delete.component.scss"],
})
export class ModalDeleteComponent implements OnInit {
  @ViewChild("autoShownModal", { static: false })
  autoShownModal: ModalDirective;

  @Input()
  isModalShown = false;

  @Input()
  title: string = "Deseja deletar o registaooaoao";

  @Input() description: string = "bem vindo Gabriel";

  showModal(): void {
    this.isModalShown = true;
  }

  hideModal(): void {
    this.autoShownModal.hide();
  }

  onHidden(): void {
    this.isModalShown = false;
  }

  constructor() {}

  ngOnInit(): void {}
}
