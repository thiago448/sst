import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ModalDeleteComponent } from "./modal-delete.component";
import { ModalModule } from "ngx-bootstrap/modal";

@NgModule({
  declarations: [ModalDeleteComponent],
  imports: [CommonModule, ModalModule.forRoot()],
  exports: [ModalDeleteComponent],
})
export class ModalDeleteModule {}
