import { Component } from "@angular/core";
import { AuthService } from "src/app/core/services/auth.service";

@Component({
  selector: "smart-dropdown-user",
  templateUrl: "./dropdown-user.component.html"
})
export class DropdownUserComponent {
  constructor(private authService: AuthService) {}
  user = {
    app: "SmartAdmin",
    name: "Dr. Codex Lantern",
    email: "drlantern@gotbootstrap.com",
    avatar: "avatar-admin.png"
  };

  logout() {
    this.authService.logout();
  }
}
