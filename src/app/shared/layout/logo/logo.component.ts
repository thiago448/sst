import { Component, ChangeDetectionStrategy } from "@angular/core";
import { ShortcutModalService } from "../shortcut-modal/shortcut-modal.service";
import { APP_CONFIG } from "src/app/app.config";
import { selectNavigationItems } from "src/app/store/navigation";
import { selectSettingsState } from "src/app/store/settings";
import { Store, createSelector } from "@ngrx/store";

@Component({
  selector: "smart-logo",
  templateUrl: "./logo.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogoComponent {
  logo = APP_CONFIG.logo;
  appName = APP_CONFIG.appName;
  minified: boolean;
  vm$ = this.store.select(
    createSelector(selectSettingsState, settings => ({
      minified: settings.minifyNavigation
    }))
  );

  constructor(
    public shortcut: ShortcutModalService,
    private store: Store<any>
  ) {}

  openShortcut($event: MouseEvent) {
    $event.preventDefault();
    this.shortcut.openModal();
  }
}
