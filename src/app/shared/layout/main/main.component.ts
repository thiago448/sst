import { Component, ChangeDetectionStrategy } from "@angular/core";
import { Store } from "@ngrx/store";
import { mobileNavigation } from "src/app/store/navigation";
import { APP_CONFIG } from "src/app/app.config";
import { AuthService } from "src/app/core/services/auth.service";

@Component({
  selector: "smart-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainComponent {
  app = APP_CONFIG.app;

  constructor(private store: Store<any>, private authService: AuthService) {}

  closeMobileNav($event: MouseEvent) {
    $event.preventDefault();
    this.store.dispatch(mobileNavigation({ open: false }));
  }
}
