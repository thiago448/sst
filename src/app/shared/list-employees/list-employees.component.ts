import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "smart-list-employees",
  templateUrl: "./list-employees.component.html",
  styleUrls: ["./list-employees.component.scss"],
})
export class ListEmployeesComponent implements OnInit {
  isDropupButtonsNewEmployee = false;

  @Output()
  eventAddNewEmployee = new EventEmitter<any>();

  @Output()
  eventAddExistEmployee = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}
}
