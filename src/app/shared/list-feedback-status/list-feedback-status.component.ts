import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "smart-list-feedback-status",
  templateUrl: "./list-feedback-status.component.html",
  styleUrls: ["./list-feedback-status.component.scss"]
})
export class ListFeedbackStatusComponent implements OnInit {
  @Input() title = "";

  constructor() {}

  ngOnInit() {}
}
