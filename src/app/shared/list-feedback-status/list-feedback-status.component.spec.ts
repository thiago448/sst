import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFeedbackStatusComponent } from './list-feedback-status.component';

describe('ListFeedbackStatusComponent', () => {
  let component: ListFeedbackStatusComponent;
  let fixture: ComponentFixture<ListFeedbackStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListFeedbackStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFeedbackStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
