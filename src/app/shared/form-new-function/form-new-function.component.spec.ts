import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNewFunctionComponent } from './form-new-function.component';

describe('FormNewFunctionComponent', () => {
  let component: FormNewFunctionComponent;
  let fixture: ComponentFixture<FormNewFunctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNewFunctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNewFunctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
