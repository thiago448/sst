import { ModalModule } from "ngx-bootstrap/modal";
import { FormNewGroupActivityModule } from "src/app/shared/form-new-group-activity/form-new-group-activity.module";
import { SharedModule } from "./../shared.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormNewFunctionComponent } from "./form-new-function.component";

@NgModule({
  declarations: [FormNewFunctionComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormNewGroupActivityModule,
    ModalModule.forRoot(),
  ],
  exports: [FormNewFunctionComponent],
})
export class FormNewFunctionModule {}
