import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StubClickDirective } from "./directives/stub-click.directive";
import { TooltipDirective } from "./directives/tooltip.directive";

@NgModule({
  declarations: [StubClickDirective, TooltipDirective],
  imports: [CommonModule],
  exports: [StubClickDirective, TooltipDirective]
})
export class UtilsModule {}
