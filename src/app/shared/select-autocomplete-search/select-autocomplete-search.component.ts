import { FormGroup, FormBuilder } from "@angular/forms";
import {
  Component,
  OnInit,
  HostListener,
  EventEmitter,
  Output,
  Input,
  forwardRef,
  HostBinding,
} from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { isObservable } from "rxjs";

@Component({
  selector: "smart-select-autocomplete-search",
  templateUrl: "./select-autocomplete-search.component.html",
  styleUrls: ["./select-autocomplete-search.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectAutocompleteSearchComponent),
      multi: true,
    },
  ],
})
export class SelectAutocompleteSearchComponent implements ControlValueAccessor {
  public isOpen: boolean = false;

  @Input() public labelName: string = null;

  @Input() public disabled = false;

  @Input() public placeholderName: string;

  public clickedInside: boolean = false;

  @Output() public clickEvent: EventEmitter<any> = new EventEmitter();

  @Input()
  public isSearch = false;

  @Input()
  public items: Array<any> = [];

  @Input()
  public itemsResult: Array<any> = [];

  public itemSearchSelected = null;

  @HostBinding("attr.id")
  externalId = "";

  @Input()
  set id(value: string) {
    this._ID = value;
    this.externalId = null;
  }

  get id() {
    return this._ID;
  }

  private _ID = "";

  @Input("value") _value = "";
  onChange: any = () => {};
  onTouched: any = () => {};

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  constructor() {}

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  isOpenOptions() {
    if (this.isSearch) {
      if (this.itemsResult) {
        console.log(this.itemsResult);
        return this.itemsResult.length > 0;
      }
      return false;
    }
    return this.isOpen;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  onInputValue(e) {
    const value = e.target.value;
    if (this.isSearch) {
      this.value = value;
      if (!value) {
        this.itemsResult === this.items;
        return;
      }
    }
    this.value = value;
    this.itemsResult = [];
  }

  onchangeValue(e) {
    if (this.isSearch && !this.value) {
      this.itemsResult = this.items;
    }
  }

  @HostListener("click")
  public clickInsideComponent() {
    this.clickedInside = true;
  }

  @HostListener("document:click")
  public clickOutsideComponent() {
    if (!this.clickedInside) {
      this.isOpen = false;
      this.itemsResult = [];
      if (
        this.isSearch &&
        this.itemSearchSelected &&
        this.value === this.itemSearchSelected.id
      ) {
        return;
      } else {
        this.value = "";
        this.itemSearchSelected = "";
        this.value = "";
      }
    } else {
      console.log("entrou abc");
    }
    this.clickedInside = false;
  }

  ToggleOpenDropDown() {
    if (!this.isSearch) {
      this.isOpen = !this.isOpen;
    } else {
      if (this.value === "") {
        this.itemsResult = this.items;
      }
    }
  }

  onSelectValue() {
    if (this.isSearch) {
      if (this.itemSearchSelected) {
        return this.itemSearchSelected.name;
      }
      return this.value;
    }
    const value = (this.isSearch ? this.itemsResult : this.items).find(
      (item) => item.id === this.value
    );
    if (value) {
      return value.name;
    } else {
      return this.value;
    }
  }

  onBlur() {
    this.itemsResult = [];
    if (
      this.isSearch &&
      this.itemSearchSelected &&
      this.value === this.itemSearchSelected.id
    ) {
      return;
    }

    this.value = "";
  }

  onClickItem(item) {
    this.itemSearchSelected = item;
    this.itemsResult = [];
    this.value = item.id;
    this.isOpen = false;
  }
  isInputReadOnly() {
    return !this.isSearch && !this.disabled;
  }
}
