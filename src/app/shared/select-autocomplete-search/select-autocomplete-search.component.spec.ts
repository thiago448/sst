import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectAutocompleteSearchComponent } from './select-autocomplete-search.component';

describe('SelectAutocompleteSearchComponent', () => {
  let component: SelectAutocompleteSearchComponent;
  let fixture: ComponentFixture<SelectAutocompleteSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectAutocompleteSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectAutocompleteSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
