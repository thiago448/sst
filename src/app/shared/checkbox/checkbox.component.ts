import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "smart-checkbox",
  templateUrl: "./checkbox.component.html",
  styleUrls: ["./checkbox.component.scss"],
})
export class CheckboxComponent implements OnInit {
  constructor() {}
  @Input() id = "";
  @Input() label = "";
  @Input() checked = false;
  ngOnInit() {}
}
