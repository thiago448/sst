import {
  Component,
  Input,
  ChangeDetectionStrategy,
  HostBinding,
  HostListener,
  ElementRef
} from "@angular/core";
import { NavigationItem, toggleNavSection } from "src/app/store/navigation";

import {
  trigger,
  state,
  style,
  transition,
  animate,
  group
} from "@angular/animations";
import { Store } from "@ngrx/store";
import { makeSlideInOut } from "../../utils/animations";

@Component({
  // tslint:disable-next-line:component-selector
  selector: "[smart-nav-item]",
  templateUrl: "./nav-item.component.html",
  animations: [makeSlideInOut()],
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ["./nav-item.component.scss"]
})
export class NavItemComponent {
  @Input() item: NavigationItem;
  @Input() minified: boolean;

  openMenu: boolean = false;

  @HostBinding("class.active") get active() {
    return this.item.active;
  }
  @HostBinding("class.open") get open() {
    return this.item.open;
  }
  @HostBinding("class.js-filter-hide]") get hidden() {
    return this.item.matched !== null && !this.item.matched;
  }
  @HostBinding("class.js-filter-show]") get shown() {
    return this.item.matched !== null && this.item.matched;
  }
  @HostBinding("class.nav-title]") get navTitle() {
    return this.item.navTitle;
  }

  constructor(private store: Store<any>, private eRef: ElementRef) {}

  toggleSection($event: MouseEvent) {
    $event.preventDefault();
    this.store.dispatch(toggleNavSection({ item: this.item }));
  }

  trackByFn(idx: number, item: NavigationItem) {
    return item.title + "_" + idx;
  }

  toogleMenu() {
    this.openMenu = !this.openMenu;
  }
  @HostListener("document:click", ["$event"])
  @HostListener("document:touchstart", ["$event"])
  handleOutsideClick(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.openMenu = false;
    }
  }
}
