import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNewGroupActivityComponent } from './form-new-group-activity.component';

describe('FormNewGroupActivityComponent', () => {
  let component: FormNewGroupActivityComponent;
  let fixture: ComponentFixture<FormNewGroupActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNewGroupActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNewGroupActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
