import { SharedModule } from "src/app/shared/shared.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormNewGroupActivityComponent } from "./form-new-group-activity.component";

@NgModule({
  declarations: [FormNewGroupActivityComponent],
  imports: [CommonModule, SharedModule],
  exports: [FormNewGroupActivityComponent],
})
export class FormNewGroupActivityModule {}
