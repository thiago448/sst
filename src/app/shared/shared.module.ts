import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { CheckboxModule } from "src/app/shared/checkbox/checkbox.module";
import { ButtonsModule } from "./buttons/buttons.module";
import { PanelsModule } from "src/app/shared/panels/panels.module";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ConnectConContainerComponent } from "./presentation/connect-con-container/connect-con-container.component";
import { FabShortcutComponent } from "./fab-shortcut/fab-shortcut.component";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ContainerFormComponent } from "./presentation/container-form/container-form.component";
import { ContainerCleanComponent } from "./presentation/container-clean/container-clean.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { SelectAutocompleteSearchComponent } from "./select-autocomplete-search/select-autocomplete-search.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DisplayStatusComponent } from "./display-status/display-status.component";
import { ListFeedbackStatusComponent } from "./list-feedback-status/list-feedback-status.component";
import { SlidePanelComponent } from "./slide-panel/slide-panel.component";
import { ContainerFormWithTabsComponent } from "./presentation/container-form-with-tabs/container-form-with-tabs.component";
import { ListEmployeesComponent } from "./list-employees/list-employees.component";
import { AutoCompleteSearchModule } from "./autocomplete-search/autocomplete-search.module";
import { DropdownFilterComponent } from "./dropdown-filter/dropdown-filter.component";

@NgModule({
  declarations: [
    ConnectConContainerComponent,
    FabShortcutComponent,
    ContainerFormComponent,
    ContainerCleanComponent,
    SidebarComponent,
    SelectAutocompleteSearchComponent,
    DisplayStatusComponent,
    ListFeedbackStatusComponent,
    SlidePanelComponent,
    ContainerFormWithTabsComponent,
    ListEmployeesComponent,
    DropdownFilterComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule.forRoot(),
    RouterModule,
    PanelsModule,
    ButtonsModule,
    CheckboxModule,
    AutoCompleteSearchModule,
    BsDropdownModule.forRoot(),
  ],
  exports: [
    SidebarComponent,
    ConnectConContainerComponent,
    FabShortcutComponent,
    ContainerCleanComponent,
    ContainerFormComponent,
    ContainerFormWithTabsComponent,
    SelectAutocompleteSearchComponent,
    DisplayStatusComponent,
    PanelsModule,
    ListFeedbackStatusComponent,
    SlidePanelComponent,
    ButtonsModule,
    ListEmployeesComponent,
    CheckboxModule,
    AutoCompleteSearchModule,
    DropdownFilterComponent,
  ],
})
export class SharedModule {}
