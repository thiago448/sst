import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "smart-display-status",
  templateUrl: "./display-status.component.html",
  styleUrls: ["./display-status.component.scss"]
})
export class DisplayStatusComponent implements OnInit {
  @Input()
  title: string;

  @Input()
  status = "DANGER";

  options = {
    SUCCESS: "SUCCESS",
    ALERT: "ALERT",
    DANGER: "DANGER"
  };

  constructor() {}

  ngOnInit() {}
}
