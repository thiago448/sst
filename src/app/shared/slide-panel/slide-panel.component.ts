import {
  Component,
  OnInit,
  Input,
  ContentChildren,
  TemplateRef,
  QueryList
} from "@angular/core";
import {
  trigger,
  state,
  style,
  transition,
  animate,
  group,
  query
} from "@angular/animations";

const slideAnimation = trigger("slideAnimation", [
  transition(
    ":increment",
    group([
      query(":enter", [
        style({
          transform: "translateX(100%)"
        }),
        animate("0.5s ease-out", style("*"))
      ]),
      query(":leave", [
        animate(
          "0.5s ease-out",
          style({
            transform: "translateX(-100%)"
          })
        )
      ])
    ])
  ),
  transition(
    ":decrement",
    group([
      query(":enter", [
        style({
          transform: "translateX(-100%)"
        }),
        animate("0.5s ease-out", style("*"))
      ]),
      query(":leave", [
        animate(
          "0.5s ease-out",
          style({
            transform: "translateX(100%)"
          })
        )
      ])
    ])
  )
]);

@Component({
  selector: "smart-slide-panel",
  templateUrl: "./slide-panel.component.html",
  styleUrls: ["./slide-panel.component.scss"],
  animations: [slideAnimation]
})
export class SlidePanelComponent implements OnInit {
  currentIndex = 0;
  slides = [
    { image: "assets/img00.jpg", description: "Image 00" },
    { image: "assets/img01.jpg", description: "Image 01" },
    { image: "assets/img02.jpg", description: "Image 02" },
    { image: "assets/img03.jpg", description: "Image 03" },
    { image: "assets/img04.jpg", description: "Image 04" }
  ];

  constructor() {}
  @ContentChildren(TemplateRef)
  templateRefs: QueryList<any>;

  ngOnInit() {
    console.log(this.templateRefs);
  }

  setCurrentSlideIndex(index) {
    this.currentIndex = index;
  }

  isCurrentSlideIndex(index) {
    return this.currentIndex === index;
  }

  prevSlide() {
    this.currentIndex =
      this.currentIndex < this.templateRefs.length - 1
        ? ++this.currentIndex
        : this.currentIndex;
  }

  nextSlide() {
    this.currentIndex =
      this.currentIndex > 0 ? --this.currentIndex : this.currentIndex;
  }
}
