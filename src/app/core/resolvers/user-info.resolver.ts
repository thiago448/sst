import { Store } from "@ngrx/store";
import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { Observable } from "rxjs";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../services/auth.service";
import { updateUser } from "src/app/store/user";
import { UsuarioModel } from "../models/usuario-model";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root",
})
export class UserInfoResolver implements Resolve<any> {
  constructor(
    private authService: AuthService,
    public toastr: ToastrService,
    private store: Store<any>
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    this.authService.getUserInfo().subscribe(
      (data) => {
        let user = new UsuarioModel();
        user = Object.assign(user, data);
        this.toastr.success(
          "Bom trabalho",
          `Bem-vindo ${user.nome} !`
        );
        this.store.dispatch(
          updateUser({
            user,
          })
        );
      },
      (error: HttpErrorResponse) => {
        if (error.status === 0) {
          this.toastr.error(
            "Verifique sua conexão com a internet",
            "Erro ao buscar informações do usuário"
          );
        }
      }
    );
  }
}
