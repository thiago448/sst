import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from "@angular/common/http";
import { Observable, throwError, BehaviorSubject } from "rxjs";
import { catchError, filter, take, switchMap, finalize } from "rxjs/operators";
import { AuthService } from "../services/auth.service";
import { BusyService } from "../services/busy.service";

@Injectable()
export class HttpError implements HttpInterceptor {
  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );
  constructor(
    private authService: AuthService,
    private busyService: BusyService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.busyService.busy();
    return next.handle(request).pipe(
      finalize(() => this.busyService.idle()),
      catchError((error) => {
        if (error.status === 401 && !this.isRefreshing) {
          return this.handle401Error(request, next);
        } else {
          return throwError(error);
        }
      })
    );
  }
  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`,
      },
    });
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    const hasRefreshToken = this.authService.hasCurrentRefreshToken();
    if (
      !this.isRefreshing ||
      hasRefreshToken ||
      request.url !== this.authService.AUTH_RESOURCE
    ) {
      if (!this.isRefreshing) {
        this.isRefreshing = true;
        this.refreshTokenSubject.next(null);
        return this.authService.tokenRefresh().pipe(
          switchMap((token: any) => {
            this.isRefreshing = false;
            this.refreshTokenSubject.next(token.access_token);
            return next.handle(this.addToken(request, token.access_token));
          }),
          catchError((error) => {
            this.authService.logout();
            return throwError(error);
          }),
          finalize(() => {
            this.isRefreshing = false;
          })
        );
      } else {
        return this.refreshTokenSubject.pipe(
          filter((token) => token != null),
          take(1),
          switchMap((jwt) => {
            return next.handle(this.addToken(request, jwt));
          })
        );
      }
    } else {
      this.authService.logout();
    }
  }
}
