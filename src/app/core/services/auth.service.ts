import { tap, timeout } from "rxjs/operators";
import { Router } from "@angular/router";
import { environment } from "./../../../environments/environment";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { AuthModel } from "../models/auth-model";
import { TokenModel } from "../models/token-model";
import { RecuperaContaOuSenhaModel } from "../models/recupera-conta-ou-senha-model";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private readonly TOKEN: string = "@sst@authtoken";
  private readonly REFRESH_TOKEN: string = "@sst@refreshtoken";
  public readonly AUTH_RESOURCE = "/oauth/token";
  private readonly USER_INFO_RESOURCE = "/v1/usuarios/self";
  private readonly USER_RECOVERY_PASSWORD_RESOURCE = "/usuarios/emails/senha";
  private readonly USER_RECOVERY_ACCOUNT_RESOURCE = "/usuarios/emails/login";

  private tokenSubject = new BehaviorSubject<string>(this.getToken());
  public currentToken: Observable<string>;

  private refreshTokenSubject = new BehaviorSubject<string>(
    this.getRefreshToken()
  );
  public refreshToken: Observable<string>;

  constructor(private httpClient: HttpClient, private router: Router) {
    this.currentToken = this.tokenSubject.asObservable();
    this.refreshToken = this.refreshTokenSubject.asObservable();
  }

  public get currentUserValue(): string {
    return this.tokenSubject.value;
  }
  public hasCurrentRefreshToken(): boolean {
    return !!this.tokenSubject.value;
  }

  private getToken(): string {
    return localStorage.getItem(this.TOKEN);
  }

  private getRefreshToken(): string {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  refreshTokenService(): any {
    return null;
  }

  login(authModel: AuthModel): Observable<any> {
    const options = this.getDefaultHeaders();
    const params = this.createHttpParams(authModel);
    return this.httpClient.post<TokenModel>(
      `${environment.urlSSO}${this.AUTH_RESOURCE}`,
      params.toString(),
      options
    );
  }

  tokenRefresh() {
    const options = this.getDefaultHeaders();
    const params = this.createHttpParamsRefresh(this.getRefreshToken());
    return this.httpClient
      .post<TokenModel>(
        `${environment.urlSSO}${this.AUTH_RESOURCE}`,
        params.toString(),
        options
      )
      .pipe(
        tap((tokenResponse) => {
          let token: TokenModel = new TokenModel();
          token = Object.assign(token, tokenResponse);
          this.updateToken(token.access_token);
          this.updateRefreshToken(token.refresh_token);
        })
      );
  }
  getUserInfo(): Observable<any> {
    const options = { headers: this.getAuthHeaders() };
    return this.httpClient.get<any>(
      `${environment.urlSSO}${this.USER_INFO_RESOURCE}`,
      options
    );
  }

  updateToken(value: string = undefined): void {
    if (!value) {
      localStorage.removeItem(this.TOKEN);
      this.tokenSubject.next(null);
      return;
    }
    localStorage.setItem(this.TOKEN, value);
    this.tokenSubject.next(value);
  }

  updateRefreshToken(value: string = undefined): void {
    if (!value) {
      localStorage.removeItem(this.REFRESH_TOKEN);
      this.tokenSubject.next(null);
      return;
    }
    localStorage.setItem(this.REFRESH_TOKEN, value);
    this.refreshTokenSubject.next(value);
  }

  isLogged() {
    return !!this.getToken();
  }

  getAuthHeaders(): HttpHeaders {
    return new HttpHeaders({
      Accept: "application/json",
      Authorization: "Bearer " + this.getToken(),
    });
  }

  private getDefaultHeaders(): any {
    return {
      headers: new HttpHeaders({
        "Content-type": "application/x-www-form-urlencoded; charset=utf-8",
      }),
    };
  }

  private createHttpParams(authModel: AuthModel): HttpParams {
    return new HttpParams()
      .append("client_id", environment.client_id)
      .append("client_secret", environment.client_secret)
      .append("grant_type", environment.grant_type)
      .append("username", authModel.username)
      .append("password", authModel.password);
  }

  private createHttpParamsRefresh(refreshToken): HttpParams {
    return new HttpParams()
      .append("client_id", environment.client_id)
      .append("client_secret", environment.client_secret)
      .append("grant_type", environment.grant_type_refresh)
      .append("refresh_token", refreshToken);
  }

  private createHttpParamsRecovery(
    data: RecuperaContaOuSenhaModel
  ): HttpParams {
    return new HttpParams()
      .append("codigoOuApelido", data.codigoOuApelido)
      .append("login", data.login)
      .append("cpf", data.cpf)
      .append("enviarCopia", data.enviarCopia);
  }

  logout(): void {
    this.updateRefreshToken();
    this.updateToken();
    this.router.navigate(["/login"]);
  }

  recoveryAccount(data: RecuperaContaOuSenhaModel): Observable<any> {
    const params = this.createHttpParamsRecovery(data);
    const options = this.getDefaultHeaders();
    return this.httpClient.post(
      `${environment.urlSSO}${this.USER_RECOVERY_ACCOUNT_RESOURCE}`,
      params.toString(),
      options
    );
  }
  recoveryPassword(data: RecuperaContaOuSenhaModel) {
    const params = this.createHttpParamsRecovery(data);
    const options = this.getDefaultHeaders();
    return this.httpClient.post(
      `${environment.urlSSO}${this.USER_RECOVERY_PASSWORD_RESOURCE}`,
      params.toString(),
      options
    );
  }
}
