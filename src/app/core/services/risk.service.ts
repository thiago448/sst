import { HttpClient, HttpBackend } from "@angular/common/http";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class RiskService {
  private readonly BASE_URL =
    "https://5eb55d2cde5849001638b56b.mockapi.io/cont/";

  private readonly RESOURCE_USER_SEACH = "users";
  private httpBackEnd: HttpClient;

  constructor(private httpClient: HttpClient, http: HttpBackend) {
    this.httpBackEnd = new HttpClient(http);
  }

  public searchUsers(page = 1, limit = 20): Observable<any> {
    return this.httpBackEnd.get(
      `${this.BASE_URL}${this.RESOURCE_USER_SEACH}?page=${page}&limit=${limit}`
    );
  }
}
