import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { Md5 } from "ts-md5/dist/md5";
@Injectable({
  providedIn: "root"
})
export class CepService {
  constructor(private httpClient: HttpClient) {}

  getCEP(cep: string): Observable<any> {
    const md5Has = Md5.hashStr(`${cep}${environment.URLCEP_PARAM}`);
    return this.httpClient.get<any>(`${environment.URLCEP}/${cep}/${md5Has}`);
  }
}
