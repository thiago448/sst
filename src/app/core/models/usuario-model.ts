import { Identidade } from "./identidade-model";
import { PessoaFisica } from "./pessoa-fisica-model";
import { FotoPainel } from "./foto-painel-model";

export class UsuarioModel {
  responsavelFinanceiro: boolean;
  dataAlteracao: Date;
  grupo: string;
  emailPrincipal: string;
  master: boolean;
  senha: string;
  dataExpiracaoConta: Date; // Sem hora
  usuarioId: string;
  tipo: string;
  situacao: string;
  migrando: boolean;
  bloqueado: boolean;
  dataExpiracaoCredencial: Date;
  nomeFantasia: string;
  identidade: Identidade;
  pessoaFisica: PessoaFisica;
  nome: string;
  foto: FotoPainel;
}
