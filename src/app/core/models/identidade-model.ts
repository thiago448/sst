export class Identidade {
  clienteId: number;
  usuarioSemApelido: string;
  apelidoCliente: string;
}
