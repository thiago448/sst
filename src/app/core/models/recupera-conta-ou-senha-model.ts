export class RecuperaContaOuSenhaModel {
  codigoOuApelido?: string;
  login?: string;
  cpf?: string;
  enviarCopia?: string;
}
