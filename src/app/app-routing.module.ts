import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainComponent } from "./shared/layout/main/main.component";
import { LayoutModule } from "./shared/layout/layout.module";
import { AuthGuard } from "./core/guards/auth.guard";
import { UserInfoResolver } from "./core/resolvers/user-info.resolver";

const routes: Routes = [
  {
    path: "login",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("src/app/features/sign-in/sign-in.module").then(
        (m) => m.SignInModule
      ),
  },
  {
    path: "recuperar-senha",
    loadChildren: () =>
      import("src/app/features/recovery/recovery.module").then(
        (m) => m.RecoveryModule
      ),
  },
  {
    path: "",
    component: MainComponent,
    canActivate: [AuthGuard],
    resolve: {
      userInfo: UserInfoResolver,
    },
    data: { requiresLogin: true },

    children: [
      { path: "", redirectTo: "intel", pathMatch: "full" },
      {
        path: "clientes",
        data: { breadcrumbs: ["Meus Clientes"] },
        loadChildren: () =>
          import("src/app/features/customers/customers.module").then(
            (m) => m.CustomersModule
          ),
      },
      {
        path: "cadastros",
        data: { breadcrumbs: ["Cadastros"] },
        loadChildren: () =>
          import("src/app/features/registrations/registrations.module").then(
            (m) => m.RegistrationsModule
          ),
      },
      {
        path: "documentos",
        data: { breadcrumbs: ["Documentos"] },
        loadChildren: () =>
          import("src/app/features/documents/documents.module").then(
            (m) => m.DocumentsModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [LayoutModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
