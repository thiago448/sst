export const NavigationItems = [
  {
    title: "Dashboard",
    tags: "application lallala",
    icon: "icon-dashboard",
    items: [
      {
        title: "Introduction",
        tags: "application intel introduction",
        routerLink: "/clientes",
      },
      {
        title: "Privacy",
        tags: "application intel privacy",
        routerLink: "/clientes",
      },
    ],
  },
  {
    title: "Meus Clientes",
    tags: "theme settings",
    icon: "icon-my-group",
    routerLink: "/clientes",
  },
  {
    title: "Cadastros",
    icon: "icon-new-form",
    tags: "icon-new-form",
    routerLink: "/settings/layout-options",
    items: [
      {
        title: "Riscos",
        routerLink: "/cadastros/riscos",
      },
      {
        title: "Efeitos/Danos",
        routerLink: "/cadastros/efeitos",
      },
    ],
  },
  {
    title: "Documentos",
    tags: "package info",
    icon: "icon-docs",
    routerLink: "/settings/layout-options",
    items: [
      {
        title: "PPRA",
        tags: "application intel introduction",
        routerLink: "/documentos/ppra",
      },
      {
        title: "Ordem de Serviço",
        tags: "application intel introduction",
        routerLink: "/documentos/ordem-servico",
      },
      {
        title: "PCMSO",
        tags: "application intel introduction",
        routerLink: "/documentos/pcmso",
      },
      {
        title: "ASO",
        tags: "application intel introduction",
        routerLink: "/documentos/aso",
      },
    ],
  },
  {
    title: "Relatórios",
    routerLink: "/ui/badges",
    icon: "icon-charts",
  },
];
