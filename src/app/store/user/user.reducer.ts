import { Action, createReducer, on } from "@ngrx/store";
import * as UserActions from "./user.action";
import { UsuarioModel } from "src/app/core/models/usuario-model";

export const initialState: UsuarioModel = new UsuarioModel();
const userReducer = createReducer(
  initialState,
  on(UserActions.updateUser, (state, action) => ({
    ...state,
    ...action.user
  }))
);

export function reducer(state: UsuarioModel, action: Action) {
  return userReducer(state, action);
}
