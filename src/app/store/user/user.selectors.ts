import { createFeatureSelector } from "@ngrx/store";
import { UsuarioModel } from "src/app/core/models/usuario-model";

export const selectUserState = createFeatureSelector<UsuarioModel>("user");
