import { createAction, props } from "@ngrx/store";
import { UsuarioModel } from "src/app/core/models/usuario-model";
export const updateUser = createAction(
  "[User] Update",
  props<{ user: UsuarioModel }>()
);
